import { ROLE_SUCCESS, ROLE_SUCCESS } from "../actions/constants";

const initialState = {
	loading: false
};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case ROLE_SUCCESS:
			return {
				...state,
				...payload,
				loading: false
			};
		case ROLE_FAIL:
			return {
				...state,
				loading: false
			};
		default:
			return state;
	}
}
