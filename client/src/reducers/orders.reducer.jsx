import { GET_ORDERS, ORDER_ERROR, PROCEED_TO_PAY,GET_ORDER_DETAILS } from "../actions/constants";

const initialState = {
	orders: [],
	loading: true,
	order: {}
};

export default function (state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case GET_ORDERS:
			return {
				...state,
				orders: payload.result,
				loading: false
			};
		case GET_ORDER_DETAILS: {
			return {
				...state,
				order: payload.result,
				loading: false
			}
		}
		case PROCEED_TO_PAY:
			return {
				...state,
				url_data: payload,
				loading: false,
				url: true
			};
		case ORDER_ERROR:
			return {
				...state,
				loading: false
			};
		default:
			return state;
	}
}
