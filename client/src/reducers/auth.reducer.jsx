import {
	REGISTER_SUCCESS,
	REGISTER_FAIL,
	LOGIN_FAIL,
	LOGIN_SUCCESS,
	LOGOUT,
	USER_LOADED,
	AUTH_ERROR,
	UPDATE_SUCCESS,
	FORGOT_PASSWORD
} from "../actions/constants";

const initialState = {
	token: localStorage.getItem("token"),
	isAuthenticated: null,
	isVerified: null,
	loading: true,
	user: null,
	loggedOut: false
};

export default function (state = initialState, action) {
	const { type, payload, edit } = action;
	switch (type) {
		case USER_LOADED:
			return {
				...state,
				isAuthenticated: true,
				loading: false,
				user: payload.result.user,
				loggedOut: false
			};
		case UPDATE_SUCCESS:
			return {
				...state,
				isAuthenticated: true,
				isVerified: true,
				loading: false,
				user: payload.result,
				edit: edit,
				loggedOut: false
			};
		case FORGOT_PASSWORD:
			return {
				...state,
				isAuthenticated: true
			};
		case LOGIN_SUCCESS:
			localStorage.setItem("token", payload.result.token);
			console.log(payload,"paylaods")
			return {
				...state,
				user: payload.result.user,
				isAuthenticated: true,
				loading: false,
				loggedOut: false
			};
		case REGISTER_SUCCESS:
			return {
				...state,
				...payload,
				isAuthenticated: true,
				loading: false,
				isVerified: payload.result.user.isVerified
			};
		case REGISTER_FAIL:
		case LOGIN_FAIL:
		case LOGOUT:
		case AUTH_ERROR:
			console.log('TOKEN RMOEVD')
			localStorage.removeItem("token");
			return {
				...state,
				token: null,
				loggedOut: payload,
				isAuthenticated: false,
				isVerified: false,
				loading: false
			};
		default:
			return state;
	}
}
