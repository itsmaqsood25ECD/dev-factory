import { GET_APPLICATION, LOAD_APPLICATION} from "../actions/constants";

const initialState = {
	Application: null,
	loading: true,
	Applications: null,

};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case LOAD_APPLICATION:
			return {
				...state,
				Applications: payload,
				loading: false
			};
		case GET_APPLICATION:
			return {
				...state,
				Application: payload,
				loading: false
			};		
		default:
			return state;
	}
}
