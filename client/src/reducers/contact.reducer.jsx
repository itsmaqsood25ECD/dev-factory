import {
  CONTACT_FAIL,
  CONTACT_SUCCESS,
  CUSTOM_APP_SUCCESS,
  CUSTOM_APP_FAIL,
  GET_IN_TOUCH_FAIL,
  GET_IN_TOUCH_SUCCESS
} from "../actions/constants";

const initialState = {
  // isSubmitted: null,
  loading: false,
  allcontact: [],
  customApp: {},
  getInTouch: {}
};

export default function(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case CONTACT_SUCCESS:
      return {
        ...state,
        ...payload,
        loading: false
      };
    case GET_IN_TOUCH_FAIL:
    case CUSTOM_APP_FAIL:
    case CONTACT_FAIL:
      return {
        ...state,
        // isSubmitted: false,
        loading: false
      };
    case CUSTOM_APP_SUCCESS: {
      return {
        ...state,
        customApp: payload.result,
        loading: false
      };
    }
    case GET_IN_TOUCH_SUCCESS:
      return {
        ...state,
        getInTouch: payload.result,
        loading: false
      };
    default:
      return state;
  }
}
