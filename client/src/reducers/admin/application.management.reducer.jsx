import {
	ALL_APPLICATION_SUCCESS,
	APPLICATION_FAIL,
	APPLICATION_SUCCESS,
	APPLICATION_LOAD, DELETE_SUCCESS,
	DELETE_FAIL
} from "../../actions/constants";

const initialState = {
	Application: null,
	loading: true,
	Applications: null,
	refresh: false
};
export default function (state = initialState, action) {
	const { type, payload, edit } = action;
	switch (type) {
		case APPLICATION_SUCCESS:
		case DELETE_FAIL:
			console.log('Application')
			return {
				...state,
				Application: payload,
				loading: false,
				refresh: true
			};
		case ALL_APPLICATION_SUCCESS:
			return {
				...state,
				Applications: payload.result,
				loading: false,
				refresh: false
			};
		case APPLICATION_LOAD:
			return {
				...state,
				Application: payload,
				loading: false
			};
		case APPLICATION_FAIL:
			return {
				...state,
				Application: null,
				Applications: null
			};
		case DELETE_SUCCESS:
			return {
				...state,
				refresh: true
			}
		default:
			return state;
	}
}
