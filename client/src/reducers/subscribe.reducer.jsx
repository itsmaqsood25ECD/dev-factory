import {GET_SUBS, SUBS_SUCCESS, SUBS_FAIL } from "../actions/constants";

const initialState = {
  loading: true,
  subscription: []
};

export default function(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_SUBS:
      console.log(payload)
      return {
        ...state,
        ...payload,
        subscription:payload.result,
        loading: false,
      };
      console.log()
    case SUBS_SUCCESS:
      return {
        ...state,
        ...payload,
        // isSubmitted: true,
        loading: false
      };
    case SUBS_FAIL:
      return {
        ...state,
        // isSubmitted: false,
        loading: false
      };
    default:
      return state;
  }
}
