import React, { Fragment, useState } from "react";
import { Row, Col } from "antd";
import { RightOutlined } from '@ant-design/icons'
import "antd/dist/antd.css";

import "../assets/css/base/custom.css";
import "../assets/css/base/main.css";
import "../assets/css/base/homeNew.css";
import "../assets/css/base/home.css";

// this page css
import "../assets/css/home.css";
import "../assets/css/my-theme-antd.css";

// images
import heroimg from '../assets/img/heroimg.png'
import img1 from '../assets/img/img1.webp'

import "slick-carousel/slick/slick.css";

// Import Other Pages
import Products from '../components/Products'

import { useTranslation } from 'react-i18next';

const HomeMain = () => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      <div className="homeMain">
        {/* ===================
                    Start Landing  
                ===================  */}
        <section className="heroMain">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle" className="outer_Row" >
              <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="heroHeading">
                {/* <h1>factory</h1> */}
                <h1>{t('FacP_home_factory')}</h1>
                <hr />
              </Col>
              <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="textCol">
                {/* <p>Get Ready To Launch Online Solutions, for your eCommerce, Marketplace, Restaurant, and Chatbot for your Business</p> */}
                <p>{t('FacP_home_herotext')}</p>
              </Col>
              <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 20, order: 3 }} className="imgCol">
                <img src={heroimg} />
              </Col>
            </Row>
          </div>
          <div className="fullWidthSection">
            <Row type="flex" align="middle" className="fullWidthOuter">
              <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 14, order: 1 }} className="fullWidthcontentCol">
                {/* <h1>Let's defeat<br />COVID-19 together!</h1> */}
                <h1>{t('FacP_home_section2_head_line1')}<br />{t('FacP_home_section2_head_line2')}</h1>
                <hr />
                {/* <p>Social distancing will not stop for upcoming few months,<br />Let your business also not stop.</p> */}
                <p>{t('FacP_home_section2_text_line1')}<br />{t('FacP_home_section2_text_line2')}</p>
                {/* <a className="exploreBtn">Explore Now <RightOutlined /></a> */}
              </Col>
              <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 10, order: 2 }} className="fullWidthImgCol">
                <img src={img1} />
              </Col>
            </Row>
          </div>
        </section>
        {/* ===================
                    End Landing  
                ===================  */}
        {/* ===================
                    Start Section 2   
                ===================  */}
        <Products />
        {/* ===================
                    End Section 2  
                ===================  */}
      </div>
    </React.Fragment>
  );
};

export default HomeMain;
