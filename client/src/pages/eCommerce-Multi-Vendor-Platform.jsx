import React, { Fragment, useState } from "react";
import { Row, Col, Button } from "antd";
import { Helmet } from "react-helmet";
import "antd/dist/antd.css";


// this page css
import "slick-carousel/slick/slick.css";
import "../assets/css/home.css";
import "../assets/css/multivendor.css";
import "../assets/css/base/singleVendor.css"
import "../assets/css/base/home.css";
import "../assets/css/base/main.css";
import "../assets/css/base/homeNew.css";
import "../assets/css/base/custom.css";
import { useTranslation } from 'react-i18next';
// Import Other Pages
import MVProducts from '../components/MultiVendorProducts'
import ContactUs from '../components/contactUs'

const HomeMain = () => {
    const { t } = useTranslation();
    return (
        <React.Fragment>
            <Helmet>
                <title>Create eCommerce Marketplace | Best Online Shopping Solution | Upapp factory</title>
                <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" />
                <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no" />
                <meta name="theme-color" content="#000000" />
                <meta property="og:description" content="Build Your Unique Online Presence" data-react-helmet="true" />
                <meta property="og:type" content="UPapp Factory" data-react-helmet="true" />
                <meta property="og:url" content="factory.upapp.co" data-react-helmet="true" />
                <meta name="description" content="Start your own online marketplace business and earn commission. A Multi-Vendor eCommerce Platform for Sellers to sell their products under One Hub." />
                <meta name="keywords" content="Multivendor Marketplace Website Development, Create eCommerce Marketplace, Online Shopping Marketplace Software" />
            </Helmet>
            <div className="multiVendorMain">
                {/* ===================
                    Start Landing  
                ===================  */}
                <section className="mv_hero_main">
                    <div className="UAF-container-fluid">
                        <Row type="flex" align="middle" className="outer_Row" >
                            <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="heroHeading">
                                <div className="sectionHeading">
                                    <h1>{t('FacP_mvp_section1_head1')}<br />{t('FacP_mvp_section1_head2')}</h1>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>
                {/* ===================
                    End Landing  
                ===================  */}
                {/* ===================
                    Start Section 2   
                ===================  */}
                <MVProducts />
                {/* ===================
                    End Section 2  
                ===================  */}

                {/* ===================
                    Start Section 3
                ===================  */}
                <ContactUs />
                {/* ===================
                    End Section 3
                ===================  */}
            </div>
        </React.Fragment>
    );
};

export default HomeMain;
