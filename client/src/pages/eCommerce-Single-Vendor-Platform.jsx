import React, { Fragment, useState, useEffect } from "react";
import { Row, Col, Button, Icon, List } from "antd";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {Link} from 'react-router-dom'
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';

import { RightOutlined } from '@ant-design/icons'
import { useTranslation } from 'react-i18next';
import "antd/dist/antd.css";
import "../assets/css/base/singleVendor.css"
import "../assets/css/base/home.css";
import "../assets/css/base/custom.css";
import "../assets/css/base/main.css";
import "../assets/css/base/homeNew.css";

// this page css
import "../assets/css/home.css";

// Mobile View

import MobilePage from '../components/MobileAppView/MobileAppPage'

// images
import heroimg from '../assets/img/singleVendor/single_vendor_featured@2x.png'
import img1 from '../assets/img/singleVendor/single-vendor.webp'
import DashboardAnalytics from '../assets/img/singleVendor/dashbord-analytics@2x.png'
import WebsiteThemes from '../assets/img/singleVendor/themes@2x.png'
import tick from '../assets/img/singleVendor/tick.svg'
import AppIconMobile from "../assets/img/singleVendor/AppIconMobile.webp"

// Icons
import HightlySecure from '../assets/img/singleVendor/Icons/HighlySecure.svg'
import MultiplePromotion from '../assets/img/singleVendor/Icons/MultiplePromotion.svg'
import NoAditionalServerCost from '../assets/img/singleVendor/Icons/HighlySecure.svg'
import OnlinePayment from '../assets/img/singleVendor/Icons/OnlinePayment.svg'
import PushNotification from '../assets/img/singleVendor/Icons/PushNotification.svg'
import QuickLoadTime from '../assets/img/singleVendor/Icons/QuickLoadTime.svg'
import ReadyToLaunch from '../assets/img/singleVendor/Icons/ReadyToLaunch.svg'
import Review from '../assets/img/singleVendor/Icons/Review.svg'
import UnlimitedTransections from '../assets/img/singleVendor/Icons/UnlimitedTransections.svg'
import "slick-carousel/slick/slick.css";

import Pricing from '../components/Pricing/SingleVendorPricing'





const ECommerceSingleVendor = ({user, direction}) => {

  const { t } = useTranslation();

  const UserFeature = [
    {
      title: `${t("singleVendor_section3_userFeature_1")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_2")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_3")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_4")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_5")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_6")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_7")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_8")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_9")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_10")}`,
    },
    {
      title: `${t("singleVendor_section3_userFeature_11")}`
    },
    {
      title: `${t("singleVendor_section3_userFeature_12")}`
    },
    {
      title: `${t("singleVendor_section3_userFeature_13")}`
    },
    {
      title: `${t("singleVendor_section3_userFeature_14")}`
    },
    {
      title: `${t("singleVendor_section3_userFeature_15")}`
    }
  ];
  
  const AdminFeatures = [
    {
      title: `${t("singleVendor_section3_adminFeature_1")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_2")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_3")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_4")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_5")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_6")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_7")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_8")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_9")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_10")}`,
    },
    {
      title: `${t("singleVendor_section3_adminFeature_11")}`
    },
    {
      title: `${t("singleVendor_section3_adminFeature_12")}`
    },
    {
      title: `${t("singleVendor_section3_adminFeature_13")}`
    }
  ];
  
  const UpAppFeatures = [
    {
      title: `${t("singleVendor_section3_upappcomp_1")}`,
    },
    {
      title: `${t("singleVendor_section3_upappcomp_2")}`,
    },
    {
      title: `${t("singleVendor_section3_upappcomp_3")}`,
    },
    {
      title: `${t("singleVendor_section3_upappcomp_4")}`,
    },
    {
      title: `${t("singleVendor_section3_upappcomp_5")}`,
    },
    {
      title: `${t("singleVendor_section3_upappcomp_6")}`,
    },
    {
      title: `${t("singleVendor_section3_upappcomp_7")}`,
    }
  ]

  const InsideTheApp = [
    {
      title: `${t("singleVendor_section4_title1")}`,
      icon: ReadyToLaunch
    },
    {
      title: `${t("singleVendor_section4_title2")}`,
      icon: PushNotification
    },
    {
      title: `${t("singleVendor_section4_title3")}`,
      icon: HightlySecure
    },
    {
      title: `${t("singleVendor_section4_title4")}`,
      icon: OnlinePayment
    },
    {
      title: `${t("singleVendor_section4_title5")}`,
      icon: QuickLoadTime
    },
    {
      title: `${t("singleVendor_section4_title6")}`,
      icon: NoAditionalServerCost
    },
    {
      title: `${t("singleVendor_section4_title7")}`,
      icon: MultiplePromotion
    },
    {
      title: `${t("singleVendor_section4_title8")}`,
      icon: UnlimitedTransections
    },
    {
      title: `${t("singleVendor_section4_title9")}`,
      icon: Review
    }
  ]


  const [appDetails, setAppData] = useState({
    apps: [],
    currency: "",
    // Booking
    bookingMobileBDSPM: "",
    bookingMobileADSPM: "",
    bookingMobileBDSPY: "",
    bookingMobileADSPY: "",
    bookingWebsiteBDSPM: "",
    bookingWebsiteADSPM: "",
    bookingWebsiteBDSPY: "",
    bookingWebsiteADSPY: "",
    bookingWebAppBDSPM: "",
    bookingWebAppADSPM: "",
    bookingWebAppBDSPY: "",
    bookingWebAppADSPY: "",
    newADSPM: "",
    newBDSPY: "",
    newADSPY: "",
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    newWebsiteBDSPM: "",
    newWebsiteADSPM: "",
    newWebsiteBDSPY: "",
    newWebsiteADSPY: "",
    newWebAppBDSPM: "",
    newWebAppADSPM: "",
    newWebAppBDSPY: "",
    newWebAppADSPY: "",
    country: "",
    // Plan B
    WFPP: "",
    WAFPP: "",
    MFPP: "",
    // setup charge
    WSC: "",
    MSC: "",
    WMSC: "",

    // ----- restaurant ------- //

    Res_newADSPM: "",
    Res_newBDSPY: "",
    Res_newADSPY: "",
    Res_newMobileBDSPM: "",
    Res_newMobileADSPM: "",
    Res_newMobileBDSPY: "",
    Res_newMobileADSPY: "",
    Res_MSC: "",
    // Plan B
    Res_MFPP: ""

  });
  const {
    country,
    currency,
    newMobileBDSPM,
    newMobileADSPM,
    newMobileBDSPY,
    newMobileADSPY,
    newWebsiteBDSPM,
    newWebsiteADSPM,
    newWebsiteBDSPY,
    newWebsiteADSPY,
    newWebAppBDSPM,
    newWebAppADSPM,
    newWebAppBDSPY,
    newWebAppADSPY,
    WFPP,
    WAFPP,
    MFPP,
    // setup charge
    WSC,
    MSC,
    WMSC,
    //Restaurnat 
    Res_newMobileBDSPM,
    Res_newMobileADSPM,
    Res_newMobileBDSPY,
    Res_newMobileADSPY,
    Res_MSC,
    Res_MFPP
  } = appDetails;

  useEffect(() => {
    window.scrollTo(0, 0);
    setAppData({
      ...appDetails,
      country: sessionStorage.getItem("country"),
      currency: sessionStorage.getItem("currency_sign"),
      // Website
      newWebsiteBDSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPM: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteBDSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      // Web App
      newWebAppBDSPM: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppBDSPY: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      // Mobile
      newMobileBDSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileBDSPY: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPY: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      //  Plan B

      WFPP: Math.round(59.9 * sessionStorage.getItem("currency_rate")),
      MFPP: Math.round(89.9 * sessionStorage.getItem("currency_rate")),
      WAFPP: Math.round(119.9 * sessionStorage.getItem("currency_rate")),
      // setup charge
      WSC: Math.round(500 * sessionStorage.getItem("currency_rate")),
      WMSC: Math.round(1200 * sessionStorage.getItem("currency_rate")),
      MSC: Math.round(800 * sessionStorage.getItem("currency_rate")),

      // Restaurant App

      // Mobile
      Res_newMobileBDSPM: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileADSPM: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileBDSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileADSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_MSC: Math.round(49.9 * sessionStorage.getItem("currency_rate")),
      //  Plan B
      Res_MFPP: Math.round(49.9 * sessionStorage.getItem("currency_rate"))
    });

  }, [sessionStorage.getItem("currency")]);


  return (
    <React.Fragment>
      <div className="homeMain">
        {/* ===================
                    Start Landing  
                ===================  */}
        <section className="heroMain">
          <div className="UAF-container-fluid">
            <Row type="flex" align="middle" className="outer_Row" >
              <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="heroHeading">
                <br /><br /> <br />
                <h1>{t("SingleVendor_heading1")} <br />
                {t("SingleVendor_heading2")}</h1>
                <hr />
                <br /><br /> <br />
              </Col>
              <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 20, order: 2 }} className="imgCol">
                <img src={heroimg} />
              </Col>
            </Row>
          </div>
          <div className="sections-container ltr">
            <Row type="flex" align="middle" className="fullWidthOuter">
              <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 14, order: 1 }} >
                <div className="section-content left">
                  <h3 className="section-heading">{t("singleVendor_section1_heading1")} <br></br> {t("singleVendor_section1_heading2")}</h3>
                  <p>{t("singleVendor_section1_desc")}</p>
                  <Button className="btn Start-Now-btn" >{t("startNow")}</Button>
                </div>
              </Col>
              <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 2 }} lg={{ span: 10, order: 2 }} className="section-image">
                <img src={img1} />
              </Col>
            </Row>
          </div>
        </section>
        {/* ===================End Landing  ===================  */}

        {/* ===================Start Section 2   ===================  */}
        <section className="heroMain" style={{ marginTop: "100px" }}>

          <div className="sectionHeading">
            <h1>{t("singleVendor_section2_heading")}</h1>
            <hr />
            <p>{t("singleVendor_section2_desc")}</p>
          </div>

          <div>
            <img className="ThemesImage" src={WebsiteThemes}></img>
          </div>
          <div style={{ textAlign: "center" }}> <Link to="/eCommerce-Single-Vendor-Platform/Website-Themes" style={{ color: "#000", fontSize: "18px" }}>{t("singleVendor_section2_CTA")}</Link> <Icon type="right" /></div>
  </section>
        {/* =================== End Section 2  ===================  */}



        {/* ================== Start Section 3    ===================  */}
        <section className="heroMain" style={{ marginTop: "100px" }}>

          <div className="sectionHeading">
            <h1>{t("singleVendor_section3_Heading")}</h1>
            <hr />
          </div>

          <div className="sectionHeading subheading" style={{ marginTop: "70px", marginBottom: "70px" }}>
            <h1>{t("singleVendor_section3_userFeature")}</h1>
            <hr />
          </div>
          <div className="UAF-container-fluid">
            <List
              size="middle"
              className=""
              grid={{
                gutter: 16,
                xs: 1,
                sm: 2,
                md: 2,
                lg: 4,
                xl: 4,
                xxl: 4
              }}
              dataSource={UserFeature}
              renderItem={item => (
                <List.Item className="featuer-list-card">
                  <p> <img src={tick} style={{ width: "25px", height: "25px" }} alt="" /> {item.title}</p>
                </List.Item>
              )}
            />
          </div>

          <div className="sectionHeading subheading" style={{ marginTop: "70px", marginBottom: "70px" }}>
            <h1>{t("singleVendor_section3_adminFeature")}</h1>
            <hr />
          </div>
          <div className="UAF-container-fluid">
            <List
              size="middle"
              className=""
              grid={{
                gutter: 16,
                xs: 1,
                sm: 2,
                md: 2,
                lg: 4,
                xl: 4,
                xxl: 4
              }}
              dataSource={AdminFeatures}
              renderItem={item => (
                <List.Item className="featuer-list-card">
                  <p> <img src={tick} style={{ width: "25px", height: "25px" }} alt="" /> {item.title}</p>
                </List.Item>
              )}
            />
          </div>

          <div className="sectionHeading subheading" style={{ marginTop: "70px", marginBottom: "70px" }}>
            <h1>{t("singleVendor_section3_upappcomp")}</h1>
            <hr />
          </div>
          <div className="UAF-container-fluid">
            <List
              size="middle"
              className=""
              grid={{
                gutter: 16,
                xs: 1,
                sm: 2,
                md: 2,
                lg: 4,
                xl: 4,
                xxl: 4
              }}
              dataSource={UpAppFeatures}
              renderItem={item => (
                <List.Item className="featuer-list-card">
                  <p> <img src={tick} style={{ width: "25px", height: "25px" }} alt="" /> {item.title}</p>
                </List.Item>
              )}
            />
          </div>


        </section>
        {/* =================== End Section 3  ===================  */}

        {/* ================== Start Section 4    ===================  */}
        <section className="heroMain" style={{ marginTop: "100px" }}>

          <div className="sectionHeading">
            <h1>What’s Inside The App</h1>
            <hr />

            <div className="UAF-container-fluid">
              <Row type="flex" align="middle" className="fullWidthOuter">
                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 14, order: 1 }} >
                  <List
                    size="middle"
                    className=""
                    grid={{
                      gutter: 16,
                      xs: 1,
                      sm: 2,
                      md: 2,
                      lg: 3,
                      xl: 3,
                      xxl: 3
                    }}
                    dataSource={InsideTheApp}
                    renderItem={item => (
                      <List.Item className="featuer-list-card">
                        <div>
                          <img src={item.icon} style={{ width: "80px", height: "80px" }} alt={item.title} />
                          <p style={{ fontSize: "16px" }}>{item.title}</p>
                        </div>

                      </List.Item>
                    )}
                  />
                </Col>
                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 2 }} lg={{ span: 10, order: 2 }} className="section-image">
                  <img style={{padding:"60px"}} src={AppIconMobile} />
                </Col>
              </Row>
            </div>

          </div>
        </section>
        {/* ================== End Section 4    ===================  */}

        <section className="heroMain" style={{ marginTop: "100px" }}>

          <div className="sectionHeading">
            <h1>{t("singleVendor_section5_heading1")}</h1>
            <hr />
          </div>

          <div className="sections-container rtl">
            <Row type="flex" align="middle" className="fullWidthOuter">

              <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="section-image ltr">
                <img src={DashboardAnalytics} />
              </Col>
              <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} >
                <div className="section-content">
                  <h3 className="section-heading">{t("singleVendor_section5_heading2")}</h3>
                  <p>{t("singleVendor_section5_desc")}</p>
                  <Button className="btn Start-Now-btn" >{t("startNow")}</Button>
                </div>
              </Col>
            </Row>
          </div>
        </section>

        {/* ================== End Section 4    ===================  */}

        <section className="heroMain" style={{ marginTop: "100px" }}>

          <div className="sectionHeading">
            <h1>{t("singleVendor_section6_heading")}</h1>
            <hr />
            <p>{t("singleVendor_section6_desc")}</p>
          </div>
          <br />
          <br />

          <MobilePage />

        </section>


        <section className="heroMain" style={{ marginTop: "0px" }}>

          <div className="sectionHeading">
            <h1>{t("singleVendor_section7_heading")} </h1>
            <hr />
            <p>{t("singleVendor_section7_desc")}</p>
          </div>
          <br />
          <br />

          <Pricing

            currency={currency}
            MBDSPM={newMobileBDSPM}
            MADSPM={newMobileADSPM}
            MBDSPY={newMobileBDSPY}
            MADSPY={newMobileADSPY}
            WBDSPM={newWebsiteBDSPM}
            WADSPM={newWebsiteADSPM}
            WBDSPY={newWebsiteBDSPY}
            WADSPY={newWebsiteADSPY}
            WABDSPM={newWebAppBDSPM}
            WAADSPM={newWebAppADSPM}
            WABDSPY={newWebAppBDSPY}
            WAADSPY={newWebAppADSPY}
            WFPP={WFPP}
            WAFPP={WAFPP}
            MFPP={MFPP}
            duration="Mo"
            PackType="Annually"
            //setup charge
            WSC={WSC}
            MSC={MSC}
            WMSC={WMSC}
            USER={user}

          />
          {console.log("user >", user)}

        </section>

        <section className="heroMain" style={{ marginTop: "100px", marginBottom: "100px" }}>

          <div className="sectionHeading">
            <h1>{t("singleVendor_closingTagline")}</h1>
          </div>
        </section>

      </div>
    </React.Fragment>
  );
};


ECommerceSingleVendor.propTypes = {
  user: PropTypes.object.isRequired,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  user: state.auth.user,
  loading: state.application.loading
});
export default connect(mapStateToProps, {
})(withDirection(ECommerceSingleVendor));
