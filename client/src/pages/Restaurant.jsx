import React, { Fragment, useState } from "react";
import { Row, Col, Button } from "antd";
import { Helmet } from "react-helmet";
import "antd/dist/antd.css";
import "../assets/css/base/home.css";
import "../assets/css/base/custom.css";
import "../assets/css/base/main.css";
import "../assets/css/base/homeNew.css";
import "slick-carousel/slick/slick.css";
import { useTranslation } from 'react-i18next';
import '../assets/css/restaurant.css'
// Import Other Pages
import ResProducts from '../components/RestaurantProducts'
import ContactUs from '../components/contactUs'
// images
import heroimg from '../assets/img/restaurant/herobg.png'
const RestaurantMain = () => {
    const { t } = useTranslation();
    return (
        <React.Fragment>
            <Helmet>
                <title>Restaurant Application for your business | Android & IOS | UPapp factory</title>
                <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" />
                <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no" />
                <meta name="theme-color" content="#000000" />
                <meta property="og:description" content="Build Your Unique Online Presence" data-react-helmet="true" />
                <meta property="og:type" content="UPapp Factory" data-react-helmet="true" />
                <meta property="og:url" content="factory.upapp.co" data-react-helmet="true" />
                <meta name="description" content="Create your own Restaurant Application in Android and iOS for your Business. Deliver a Delicious Experience With Your Own Restaurant App." />
                <meta name="keywords" content="Restaurant Application Development, Food Delivery System, Online Food Ordering platform" />
            </Helmet>
            <div className="restaurantMain">
                {/* ===================
                    Start Landing  
                ===================  */}
                <section className="res_hero_main">
                    <div className="UAF-container-fluid">
                        <Row type="flex" align="middle" className="outer_Row" >
                            <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="heroHeading">
                                <div className="sectionHeading">
                                    <h1>{t('FacP_rp_section1_head1')}<br />{t('FacP_rp_section1_head2')}</h1>
                                </div>
                            </Col>
                            <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="heroImg">
                                <img src={heroimg} />
                            </Col>
                        </Row>
                    </div>
                </section>
                {/* ===================
                    End Landing  
                ===================  */}
                {/* ===================
                    Start Section 2   
                ===================  */}
                <ResProducts />
                {/* ===================
                    End Section 2  
                ===================  */}

                {/* ===================
                    Start Section 3
                ===================  */}
                <ContactUs />
                {/* ===================
                    End Section 3
                ===================  */}
            </div>
        </React.Fragment>
    );
};

export default RestaurantMain;
