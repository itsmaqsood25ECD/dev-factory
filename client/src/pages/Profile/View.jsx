/* eslint react/prop-types: 0 */
import React, { Fragment, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Hero from "../../components/hero";
import { CountryDropdown } from "react-country-region-selector";
import {
  Row,
  Col,
  Input,
  Form,
  Typography,
  Icon,
  Button,
  Card,
  Spin
} from "antd";
import "antd/dist/antd.css";

const { Title } = Typography;
const View = ({ user, loading }) => {

  useEffect(() => {
		window.scrollTo(0, 0)
	}, []);

  return loading && user === null ? (
    <Fragment>
      <Spin />
    </Fragment>
  ) : (
    <Fragment>
      <Hero Title="My Profile" />
      <div
        className="contact-heading"
        style={{
          padding: "auto 15px"
        }}
      />
      <section>
        <Card
          title="My Profile"
          extra={
            <Link to="edit-profile">
              <Button
                type="primary"
                size={"small"}
                style={{ float: "right", margin: "0px 15px" }}
              >
                <Icon type="edit" />
                Edit
              </Button>
            </Link>
          }
          style={{
            marginBottom: "24px",
            marginLeft: "15px",
            marginRight: "15px"
          }}
        >
          <Card title="General Details">
            <section style={{ padding: "0 15px" }}>
              <Row type="flex" justify="space-around">
                <Col lg={2} xs={24}>
                  <Form.Item
                    label="First Name"
                    className="profile-form-label"
                  />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.firstName} />
                </Col>
                <Col lg={2} xs={24}>
                  <Form.Item label="Last Name" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.lastName} />
                </Col>
              </Row>
              <Row type="flex" justify="space-around">
                <Col lg={2} xs={24}>
                  <Form.Item label="Age" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.age} />
                </Col>
                <Col lg={2} xs={24}>
                  <Form.Item label="Gender" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.gender} />
                </Col>
              </Row>
              <Row type="flex" justify="space-around">
                <Col lg={2} xs={24}>
                  <Form.Item label="E-Mail" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.email} />
                </Col>
                <Col lg={2} xs={24}>
                  <Form.Item label="Phone" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.phone} />
                </Col>
              </Row>
            </section>
          </Card>
          <Card title="Address Details" style={{ marginTop: "24px" }}>
            <section>
              <Row type="flex" justify="space-around">
                <Col lg={2} xs={24}>
                  <Form.Item label="Line 1" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.line1} />
                </Col>
                <Col lg={2} xs={24}>
                  <Form.Item label="Line 2" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.line2} />
                </Col>
              </Row>
              <Row type="flex" justify="space-around">
                <Col lg={2} xs={24}>
                  <Form.Item label="City" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.city} />
                </Col>
                <Col lg={2} xs={24}>
                  <Form.Item label="State" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.state} />
                </Col>
              </Row>
              <Row type="flex" justify="space-around">
                <Col lg={2} xs={24}>
                  <Form.Item
                    label="Postal Code"
                    className="profile-form-label"
                  />
                </Col>
                <Col lg={6} xs={24}>
                  <Input disabled value={user && user.postal_code} />
                </Col>
                <Col lg={2} xs={24}>
                  <Form.Item label="Country" className="profile-form-label" />
                </Col>
                <Col lg={6} xs={24}>
                  <CountryDropdown
                    blacklist={[
                      "AF",
                      "AO",
                      "DJ",
                      "GQ",
                      "ER",
                      "GA",
                      "IR",
                      "KG",
                      "LY",
                      "MD",
                      "NP",
                      "NG",
                      "ST",
                      "SL",
                      "SD",
                      "SY",
                      "SR",
                      "TM",
                      "VE",
                      "ZW",
                      "IL"
                    ]}
                    name="country"
                    valueType="short"
                    disabled
                    value={user && user.country}
                    // onChange={(val)(val)
                  />
                  {/* <Input disabled value={user && user.country} /> */}
                </Col>
              </Row>
            </section>
          </Card>
        </Card>
      </section>
      <div
        className="contact-heading"
        style={{
          padding: "auto 15px"
        }}
      />
    </Fragment>
  );
};

View.propTypes = {
  user: PropTypes.object.isRequired,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  user: state.auth.user,
  loading: state.auth.loading
});
export default connect(mapStateToProps, null)(View);
