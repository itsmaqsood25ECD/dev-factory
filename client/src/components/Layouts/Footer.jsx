import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Layout, Row, Col, Typography } from "antd";
import { InstagramOutlined, FacebookOutlined, LinkedinOutlined } from "@ant-design/icons";
import withDirection from 'react-with-direction';
import Logo from "../../assets/img/logo.png"
import sflag from "../../assets/img/singaporeflag.png"
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
const { Title, Text } = Typography;
const { Footer } = Layout;

const FooterComp = () => {
  const { t } = useTranslation();
  const generalFooter = (
    <Fragment>
      <Footer className="app-footer" style={{ paddingTop: "100px" }}>
        <Row gutter={16} type="flex" className="fotterMain_row" align="top" style={{ margin: "0" }}>
          <Col lg={6} md={12} sm={24} xs={24} style={{ textAlign: "left" }}>
            <Link to="/">
              <img
                src={Logo}
                alt="its a footer logo"
                style={{ width: "50px", marginBottom: "10px" }}
              />
            </Link>
            <div>
              <p style={{ color: "#FFFFFF", maxWidth: "285px" }}>{t('FacP_home_footer_text')}</p>
            </div>
            <div>
              <Title className="fontSize-20" style={{ color: "#FFFFFF" }}>{t('FacP_home_footer_follow')}</Title>
            </div>
            <div className="socialIcons">
              <a target="_blank" href="https://www.instagram.com/upappfactory/"><InstagramOutlined /></a>
              <a target="_blank" href="https://www.facebook.com/UpAppFactory/"><FacebookOutlined /></a>
              <a target="_blank" href="https://www.linkedin.com/company/upappfactory/"><LinkedinOutlined /></a>
            </div>
          </Col>
          <Col lg={5} md={12} sm={24} xs={24}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">{t('FacP_home_footer_location')}</Title>
              </div>
              <div>
                <Title className="fontSize-20 location">
                  <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Country+Flag/Flag_of_Oman.svg" /> Sultanate of Oman</Title>
                <p>101 Salam Square South, Dohat Al Adab Street, Madinat Sultan Qaboos, Muscat</p>
              </div>
              <div>
                <Title className="fontSize-20 location"><img src={sflag} /> Singapore</Title>
                <p>531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore</p>
              </div>
            </div>
          </Col>
          <Col lg={4} md={12} sm={24} xs={12}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">{t('FacP_home_footer_our_products')}</Title>
              </div>
              <div className="footer-nav-item">
                <Link to="/eCommerce-Single-Vendor-Platform">
                  <Text>{t('FacP_home_footer_single')}</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/eCommerce-multi-Vendor-Platform">
                  <Text>{t('FacP_home_footer_multi')}</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/restaurant">
                  <Text>{t('FacP_home_footer_rest')}</Text>
                </Link>
              </div>
              {/* <div className="footer-nav-item">
                <a target="_blank" href="https://upappfactory.com/upappbot">
                  <Text>UPapp Bots</Text>
                </a>
              </div> */}
            </div>
          </Col>
          <Col lg={4} md={12} sm={24} xs={12}>
            <div className="footer-section">
              <div>
                <Title className="fontSize-20">{t('FacP_home_footer_Our_Company')}</Title>
              </div>
              <div className="footer-nav-item">
                <Link to="/aboutus">
                  <Text>{t('FacP_home_footer_About_Us')}</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="terms-service">
                  <Text>{t('FacP_home_footer_Terms_Service')}</Text>
                </Link>
              </div>

              <div className="footer-nav-item">
                <Link to="privacy-policy">
                  <Text>{t('FacP_home_footer_Privacy_Policy')}</Text>
                </Link>
              </div>

              <div className="footer-nav-item">
                <Link to="cancellation-and-refund-policy">
                  <Text>{t('FacP_home_footer_Cancellation_Policy')}</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/contactUs">
                  <Text>{t('FacP_home_footer_Contact_Us')}</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/clientele">
                  <Text>{t('FacP_home_footer_Clientele')}</Text>
                </Link>
              </div>
              <div className="footer-nav-item">
                <Link to="/blog">
                  <Text>{t('FacP_home_footer_Blogs')}</Text>
                </Link>
              </div>
            </div>
          </Col>
        </Row>
        <Row gutter={16} style={{ margin: 0, padding: "30px 0px 10px 0px" }}>
          <Col
            lg={24}
            md={24}
            sm={24}
            xs={24}
            style={{ padding: "5px", textAlign: "center" }}
            className="footer-copy-right"
          >
            <Link to="/">UPapp factory</Link> <span>©</span>2020 all rights are reserved
          </Col>
        </Row>
      </Footer>
    </Fragment>
  );
  return (
    <Fragment>
      {generalFooter}
    </Fragment>
  );
};


export default connect(null, null)(
  withDirection(FooterComp)
);
