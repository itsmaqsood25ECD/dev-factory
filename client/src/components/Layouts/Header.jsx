/* eslint react/prop-types: 0 */
import React, { Fragment, useState, useEffect } from "react";
import { HashLink as NavLink } from 'react-router-hash-link';
import {Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { Layout, Menu, Form, Dropdown, Drawer, Button, Icon, Switch } from "antd";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import axios from "axios";
import PropTypes from "prop-types";
import { logout } from "../../actions/auth.action";
import 'animate.css'
import "../../assets/css/base/homeNew.css";


const { Header } = Layout;

// let filteredApps;
const HeaderComp = ({ auth: { loading }, user, logout, direction }) => {



  const [headerLink, setHeaderLink] = useState({
    visible: false,
  });

  const [state, setState] = useState({
    countryCode: "",
    CurrencyView: "",
    emVisible: true,
    language: "EN",
    menuVisible:true
  });

  const { CurrencyView, countryCode, emVisible, language, menuVisible } = state;

  const toggleMenu = () => {

    setState({
      ...state,
      menuVisible: !menuVisible
    })
  }

  const { t } = useTranslation();
  const fetchData = async () => {

    try {
      console.log("This statement works")

      const response = await axios.get(
        `https://api.ipstack.com/check?access_key=32e8b62adfadea003abc9299f80931ea&format=1`
      );

      // console.log("FETCH>>>>>>>>>>>>>>>>>>>>>", response);

      console.log(response.data.currency.code);

      sessionStorage.setItem("current_country", response.data.currency.code)

      const currexc = await axios.get(
        // `https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${response.data.currency.code}&source=USD&format=1`
        `https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${response.data.currency.code},USD,AED,SGD,OMR,QAR,MYR,LKR,EGP,SAR,KWD,JOD,KYD,ZAR&source=USD&format=1`
      );
      const currexcSGD = await axios.get(
        // `https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${response.data.currency.code}&source=USD&format=1`
        `https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${response.data.currency.code},USD,AED,SGD,OMR,QAR,MYR,LKR,EGP,SAR,KWD,JOD,KYD,ZAR&source=SGD&format=1`
      );

      console.log("currexc", currexc)
      sessionStorage.setItem("exchange_rate_response", JSON.stringify(currexc))
      sessionStorage.setItem("sgd_exchange_rate_response", JSON.stringify(currexcSGD))

      const curr = `USD${response.data.currency.code}`;
      const sgdcurr = `SGD${response.data.currency.code}`;
      // console.log(00
      //   "Currency Exchange Price >>>>>",
      //   currexc.data.quotes[`${curr}`]
      // );

      sessionStorage.setItem("currency_rate", currexc.data.quotes[`${curr}`]);
      sessionStorage.setItem("sgd_currency_rate", currexcSGD.data.quotes[`${sgdcurr}`]);
      sessionStorage.setItem("currency_sign", response.data.currency.code);
      // sessionStorage.setItem("currency", response.data.currency.code);
      if (sessionStorage.getItem("currency") === "KWD") {
        setState({
          ...state,
          CurrencyView: "KWD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (sessionStorage.getItem("currency") === "AED") {
        setState({
          ...state,
          CurrencyView: "AED"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (sessionStorage.getItem("currency") === "SGD") {
        setState({
          ...state,
          CurrencyView: "SGD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "OMR") {
        setState({
          ...state,
          CurrencyView: "OMR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "QAR") {
        setState({
          ...state,
          CurrencyView: "QAR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "BHD") {
        setState({
          ...state,
          CurrencyView: "BHD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "MYR") {
        setState({
          ...state,
          CurrencyView: "MYR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "LKR") {
        setState({
          ...state,
          CurrencyView: "LKR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "EGP") {
        setState({
          ...state,
          CurrencyView: "EGP"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "SAR") {
        setState({
          ...state,
          CurrencyView: "SAR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "JOD") {
        setState({
          ...state,
          CurrencyView: "JOD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "KYD") {
        setState({
          ...state,
          CurrencyView: "KYD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "ZAR") {
        setState({
          ...state,
          CurrencyView: "ZAR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else {
        setState({
          ...state,
          CurrencyView: response.data.currency.code
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      }

      throw new Error('This statement throws an error')
    }
    catch (error) {
      console.log("Error has been handled")
      sessionStorage.clear()

      const response = await axios.get(
        `https://api.ipstack.com/check?access_key=32e8b62adfadea003abc9299f80931ea&format=1`
      );

      // console.log("FETCH>>>>>>>>>>>>>>>>>>>>>", response);

      console.log(response.data.currency.code);

      sessionStorage.setItem("current_country", response.data.currency.code)

      const currexc = await axios.get(
        // `https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${response.data.currency.code}&source=USD&format=1`
        `https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${response.data.currency.code},USD,AED,SGD,OMR,QAR,MYR,LKR,EGP,SAR,KWD,JOD,KYD,ZAR&source=USD&format=1`
      );
      const currexcSGD = await axios.get(
        // `https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${response.data.currency.code}&source=USD&format=1`
        `https://apilayer.net/api/live?access_key=342884b134d8ad7ec3f4d3887330646c&currencies=${response.data.currency.code},USD,AED,SGD,OMR,QAR,MYR,LKR,EGP,SAR,KWD,JOD,KYD,ZAR&source=SGD&format=1`
      );

      console.log("currexc", currexc)
      sessionStorage.setItem("exchange_rate_response", JSON.stringify(currexc))
      sessionStorage.setItem("sgd_exchange_rate_response", JSON.stringify(currexcSGD))

      const curr = `USD${response.data.currency.code}`;
      const sgdcurr = `SGD${response.data.currency.code}`;
      // console.log(00
      //   "Currency Exchange Price >>>>>",
      //   currexc.data.quotes[`${curr}`]
      // );

      sessionStorage.setItem("currency_rate", currexc.data.quotes[`${curr}`]);
      sessionStorage.setItem("sgd_currency_rate", currexcSGD.data.quotes[`${sgdcurr}`]);
      sessionStorage.setItem("currency_sign", response.data.currency.code);
      // sessionStorage.setItem("currency", response.data.currency.code);
      if (sessionStorage.getItem("currency") === "KWD") {
        setState({
          ...state,
          CurrencyView: "KWD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (sessionStorage.getItem("currency") === "AED") {
        setState({
          ...state,
          CurrencyView: "AED"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (sessionStorage.getItem("currency") === "SGD") {
        setState({
          ...state,
          CurrencyView: "SGD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "OMR") {
        setState({
          ...state,
          CurrencyView: "OMR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "QAR") {
        setState({
          ...state,
          CurrencyView: "QAR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "BHD") {
        setState({
          ...state,
          CurrencyView: "BHD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "MYR") {
        setState({
          ...state,
          CurrencyView: "MYR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "LKR") {
        setState({
          ...state,
          CurrencyView: "LKR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "EGP") {
        setState({
          ...state,
          CurrencyView: "EGP"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "SAR") {
        setState({
          ...state,
          CurrencyView: "SAR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "JOD") {
        setState({
          ...state,
          CurrencyView: "JOD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "KYD") {
        setState({
          ...state,
          CurrencyView: "KYD"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else if (response.data.currency.code === "ZAR") {
        setState({
          ...state,
          CurrencyView: "ZAR"
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      } else {
        setState({
          ...state,
          CurrencyView: response.data.currency.code
        });
        sessionStorage.setItem("currency", response.data.currency.code);
      }


    }
    finally {
      console.log("Everything has been handled")
    }


  };

  useEffect(() => {

    console.log("user >>", user)

    if (sessionStorage.getItem("currency")) {
      setState({
        ...state,
        countryCode: sessionStorage.getItem("currency"),
        CountryView: sessionStorage.getItem("currency")
      });
    } else {

      fetchData();

      if (localStorage.getItem('lng')) {
        setState({
          ...state,
          language: localStorage.getItem('lng'),

        })
      } else {
        localStorage.setItem('lng', 'EN')
        setState({
          ...state,
          language: 'EN',
        })
      }

      if (localStorage.getItem('lng') === 'AR') {
        i18next.changeLanguage('AR')
      } else if (localStorage.getItem('lng') === 'EN') {
        i18next.changeLanguage('EN')
      }
    }
  }, [countryCode, CurrencyView, user, logout]);

  const { visible } = headerLink;
  const myProductMenu = (
    <Menu>
      <Menu.Item>
        <Link to="/eCommerce-Single-Vendor-Platform">
          {/* My Profile */}
          {t("FacP_home_section3_single_title_line1")}
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/eCommerce-Multi-Vendor-Platform">
          {/* My Order */}
          {t("FacP_home_section3_multi_title_line1")}
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/restaurant">
          {/* My Order */}
          {t("FacP_home_section3_rest_title_line1")}
        </Link>
      </Menu.Item>
    </Menu>
  );

  const myAccountMenu = (
    <Menu>
      <Menu.Item>
        <Link to="/my-profile">
          {/* My Profile */}
          {t('FacP_header_myprofile')}
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/my-orders">
          {/* My Order */}
          {t('FacP_header_myorder')}
        </Link>
      </Menu.Item>
      <Menu.Item onClick={() => logoutME()}>
        {/* Logout */}
        {t('FacP_header_logout')}
      </Menu.Item>
    </Menu>
  );
  
  const languageToggle = () => {

    if (localStorage.getItem('lng') === 'AR') {
      i18next.changeLanguage('EN')
      localStorage.setItem('lng', 'EN')
      window.location.reload()
    } else if (localStorage.getItem('lng') === 'EN') {
      localStorage.setItem('lng', 'AR')
      i18next.changeLanguage('AR')
      window.location.reload()
    }
  }

  const logoutME = () => {
    logout()
    window.location.reload()
  };

  const onClose = () => {
    setHeaderLink({
      visible: false
    });
  };

  const showDrawer = () => {
    setHeaderLink({
      visible: true
    });
  };


  const generalHeader = (
    <Fragment>
      <Header className="responsive-header-menu">
        <NavLink className="brand-logo" to="/">
          <div className="logo" />
        </NavLink>
        <Button type="" style={{ fontSize: "30px", border: "2px solid" }} onClick={showDrawer} >
          <Icon type="align-right" />
        </Button>
        <Drawer title="Menu" placement={direction === DIRECTIONS.LTR ? 'left' : 'right'}
          closable={false} onClose={onClose} visible={visible}>
          <Menu className="drawer-v-Menu" theme="light" mode="vertical" defaultSelectedKeys={["0"]} onSelect={onClose}>
            <Menu.Item key="dr-factory">
              {/* <NavLink to="/factory" className="mobile-menu-name-data">About Us</NavLink> */}
              <NavLink to="/factory" className="mobile-menu-name-data">{t('FacP_header_aboutus')}</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-pricing">
              {/* <NavLink to="/pricing" className="mobile-menu-name-data">Clientele</NavLink> */}
              <NavLink to="/pricing" className="mobile-menu-name-data">{t('FacP_header_Clientele')}</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              {/* <NavLink to="/" className="mobile-menu-name-data">Consultant</NavLink> */}
              <NavLink to="/" className="mobile-menu-name-data">{t('FacP_header_Consultant')}</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              {/* <NavLink to="/" className="mobile-menu-name-data">Pricing</NavLink> */}
              <NavLink to="/" className="mobile-menu-name-data">{t('FacP_header_Pricing')}</NavLink>
            </Menu.Item>
            <Menu.Item key="dr-support">
              {/* <NavLink to="/" className="mobile-menu-name-data">Contact Us</NavLink> */}
              <NavLink to="/" className="mobile-menu-name-data">{t('FacP_header_Contactus')}</NavLink>
            </Menu.Item>
          </Menu>
        </Drawer>
      </Header>
      {direction === DIRECTIONS.LTR && <Fragment>
        <Header className="header-comp animate__animated ">
          <NavLink to="/">
            <div className="logo" />
          </NavLink>

          <div className="menuToggleButton right" onClick={toggleMenu}>
            {console.log("menuVisible", menuVisible)}
            <svg id="Group_2" data-name="Group 2" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
            <g id="Rectangle_57" data-name="Rectangle 57" transform="translate(19 19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
            <rect width="13" height="13" rx="2" stroke="none"/>
            <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none"/>
            </g>
            <g id="Rectangle_60" data-name="Rectangle 60" transform="translate(19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
            <rect width="13" height="13" rx="2" stroke="none"/>
            <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none"/>
            </g>
            <g id="Rectangle_58" data-name="Rectangle 58" transform="translate(0 19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
            <rect width="13" height="13" rx="2" stroke="none"/>
            <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none"/>
            </g>
            <g id="Rectangle_59" data-name="Rectangle 59" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
            <rect width="13" height="13" rx="2" stroke="none"/>
            <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none"/>
            </g>
            </svg>
          </div>
        { menuVisible && <Menu className="U-Menu animate__animated animate__fadeInRight" theme="light" mode="horizontal" defaultSelectedKeys={["0"]}>
          {localStorage.getItem('lng') === 'EN' ?  <Menu.Item onClick={languageToggle} key="n-factory">
              Arabic
            </Menu.Item> :  <Menu.Item onClick={languageToggle} key="n-factory">
              English
            </Menu.Item>}
           
            {/* <Menu.Item key="n-factory">
              <NavLink activeClassName="is-active" to="/aboutus">{t('FacP_header_aboutus')}</NavLink>
            </Menu.Item> */}
            <Dropdown overlay={myProductMenu}>
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                 Products <Icon type="down" />
                </a>
              </Dropdown>
            <Menu.Item >
              <NavLink to="/clientele">{t('FacP_header_Clientele')}</NavLink>
            </Menu.Item>
            <Menu.Item >
              <NavLink to="/blog">{t('FacP_header_blog')}</NavLink>
            </Menu.Item>
            <Menu.Item >
              <NavLink to="/contactUs">{t('FacP_header_Contactus')}</NavLink>
            </Menu.Item>
            {user == null ? <Menu.Item >
              <NavLink to="/login">{t('FacP_header_login')}</NavLink>
            </Menu.Item> :  <Menu.Item
              key="n-country-selector-user"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector  float-right"
            >
              <Dropdown overlay={myAccountMenu}>
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                {t('FacP_header_myaccount')} <Icon type="down" />
                </a>
              </Dropdown>

            </Menu.Item>
           }
           </Menu>
            
           
        }
          
        </Header>
      </Fragment>}
      {direction === DIRECTIONS.RTL && <Fragment>
        <Header className="header-comp float-right">
          <NavLink to="/">
            <div className="logo float-right" />
          </NavLink>
          <div className="menuToggleButton left" onClick={toggleMenu}>
         
            <svg id="Group_2" data-name="Group 2" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
            <g id="Rectangle_57" data-name="Rectangle 57" transform="translate(19 19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
            <rect width="13" height="13" rx="2" stroke="none"/>
            <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none"/>
            </g>
            <g id="Rectangle_60" data-name="Rectangle 60" transform="translate(19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
            <rect width="13" height="13" rx="2" stroke="none"/>
            <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none"/>
            </g>
            <g id="Rectangle_58" data-name="Rectangle 58" transform="translate(0 19)" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
            <rect width="13" height="13" rx="2" stroke="none"/>
            <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none"/>
            </g>
            <g id="Rectangle_59" data-name="Rectangle 59" fill={menuVisible === true ? "#333" : "none"} stroke="#333" stroke-width="2">
            <rect width="13" height="13" rx="2" stroke="none"/>
            <rect x="1.5" y="1.5" width="10" height="10" rx="0.5" fill="none"/>
            </g>
            </svg>
          </div>
        
          { menuVisible && <Menu className="U-Menu animate__animated animate__fadeInLeft" theme="light" mode="horizontal" defaultSelectedKeys={["0"]}>
            
          {localStorage.getItem('lng') === 'EN' ?  <Menu.Item onClick={languageToggle} key="n-factory">
              Arabic
            </Menu.Item> :  <Menu.Item onClick={languageToggle} key="n-factory">
              English
            </Menu.Item>}
            <Menu.Item >
              <NavLink activeClassName="is-active" to="/aboutus">{t('FacP_header_aboutus')}</NavLink>
            </Menu.Item>
            <Menu.Item>
              <NavLink to="/clientele">{t('FacP_header_Clientele')}</NavLink>
            </Menu.Item>
            <Menu.Item>
              <NavLink to="/blog">{t('FacP_header_blog')}</NavLink>
            </Menu.Item>
            <Menu.Item>
              <NavLink to="/contactUs">{t('FacP_header_Contactus')}</NavLink>
            </Menu.Item>
            {user == null ? <Menu.Item >
              <NavLink to="/login">{t('FacP_header_login')}</NavLink>
            </Menu.Item> :  <Menu.Item
              key="n-country-selector-user"
              style={{ padding: "0px" }}
              className="d-guestLinks cc-selector  float-left"
            >
              <Dropdown overlay={myAccountMenu}>
                <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                {t('FacP_header_myaccount')} <Icon type="down" />
                </a>
              </Dropdown>

            </Menu.Item>
           }
          </Menu> }
        
        </Header>
      </Fragment>}
    </Fragment>
  );

  return (
    <Fragment>
      {generalHeader}
    </Fragment>
  );
};

HeaderComp.prototypes = {
  direction: withDirectionPropTypes.direction,
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  user: state.auth.user
});

export default connect(mapStateToProps, { logout })(withDirection(HeaderComp));
