import React from "react";
import { Row, Col, Button, Card, Icon } from "antd";
import { RightOutlined, MinusOutlined } from '@ant-design/icons'
import SSsliderFacility from "../pages/SSsliderFacility";
import '../assets/css/product.css'
import '../assets/css/mobilescreenSlider.css'
import { useTranslation } from 'react-i18next';
import "slick-carousel/slick/slick.css";
import img1 from '../assets/img/restaurant/img1.png'
import img2 from '../assets/img/restaurant/img2.png'
import img3 from '../assets/img/restaurant/img3.png'
import img4 from '../assets/img/restaurant/img4.png'
import img5 from '../assets/img/restaurant/img5.png'
import img6 from '../assets/img/restaurant/img6.png'
import img7 from '../assets/img/restaurant/img7.png'
import img8 from '../assets/img/restaurant/img8.png'
import img9 from '../assets/img/restaurant/img9.png'
import img10 from '../assets/img/restaurant/img10.png'
import img11 from '../assets/img/restaurant/img11.png'
import fimg1 from '../assets/img/restaurant/fimg1.png'
import fimg2 from '../assets/img/restaurant/fimg2.png'
import fimg3 from '../assets/img/restaurant/fimg3.png'
import fimg4 from '../assets/img/restaurant/fimg4.png'
import fimg5 from '../assets/img/restaurant/fimg5.png'
import fimg6 from '../assets/img/restaurant/fimg6.png'
import af1 from '../assets/img/restaurant/af1.svg'
import af2 from '../assets/img/restaurant/af2.svg'
import af3 from '../assets/img/restaurant/af3.svg'
import scr1 from '../assets/img/restaurant/scr1.png'
import scr2 from '../assets/img/multivendor/scr2.png'
import scr3 from '../assets/img/multivendor/scr3.png'
import scr4 from '../assets/img/multivendor/scr4.png'
import scr5 from '../assets/img/multivendor/scr5.png'


const ResProducts = () => {
    const { t } = useTranslation();
    const mvfeatures = [
        {
            order: '1',
            img: fimg1,
            title: `${t("FacP_rp_section5_card1_head")}`,
            desc: `${t("FacP_rp_section5_card1_text")}`
        },
        {
            order: '2',
            img: fimg2,
            title: `${t("FacP_rp_section5_card2_head")}`,
            desc: `${t("FacP_rp_section5_card2_text")}`
        },
        {
            order: '3',
            img: fimg3,
            title: `${t("FacP_rp_section5_card3_head")}`,
            desc: `${t("FacP_rp_section5_card3_text")}`
        },
        {
            order: '4',
            img: fimg4,
            title: `${t("FacP_rp_section5_card4_head")}`,
            desc: `${t("FacP_rp_section5_card4_text")}`
        },
        {
            order: '5',
            img: fimg5,
            title: `${t("FacP_rp_section5_card5_head")}`,
            desc: `${t("FacP_rp_section5_card5_text")}`
        },
        {
            order: '6',
            img: fimg6,
            title: `${t("FacP_rp_section5_card6_head")}`,
            desc: `${t("FacP_rp_section5_card6_text")}`
        }
    ]
    const addfeatures = [
        {
            order: '1',
            img: af1,
            title: `${t("FacP_rp_section7_head1")}`,
            desc: `${t("FacP_rp_section7_text1")}`
        },
        {
            order: '2',
            img: af2,
            title: `${t("FacP_rp_section7_head2")}`,
            desc: `${t("FacP_rp_section7_text2")}`
        },
        {
            order: '3',
            img: af3,
            title: `${t("FacP_rp_section7_head3")}`,
            desc: `${t("FacP_rp_section7_text3")}`
        }
    ]

    const mstore_Screenshot = [
        {
            img: scr1,
        },
        {
            img: scr2,
        },
        {
            img: scr3,
        },
        {
            img: scr4,
        },
        {
            img: scr5,
        },
        {
            img: scr1,
        },
        {
            img: scr2,
        },
        {
            img: scr3,
        }
    ];
    return (
        <React.Fragment>
            <div >
                <div className="mvp_Main">
                    <Row type="flex" align="middle" className="UAF-container-fluid">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 20, order: 1 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_rp_section2_head')}</h1>
                                        <p>{t('FacP_rp_section2_text')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img1} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 20, order: 2 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} className="rtl maintextright">
                                    <div>
                                        <h1>{t('FacP_rp_section3_head')}</h1>
                                        <p>{t('FacP_rp_section3_text')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgleft">
                                    <img src={img2} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 20, order: 3 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_rp_section4_head')}</h1>
                                        <p>{t('FacP_rp_section4_text')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img3} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row type="flex" align="middle" className="resproductFeature">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 20, order: 1 }} type="flex" align="middle" className="UAF-container-fluid sectionHeading">
                            <h1 className="colordark">{t('FacP_rp_section5_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 19, order: 2 }} type="flex" align="middle" className="UAF-container-fluid resFeatureColMain">
                            <Row type="flex" align="middle" gutter={16} className="ltr">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 16, order: 1 }}>
                                    <Row type="flex" gutter={16} className="ltr">
                                        {mvfeatures.map(index => {
                                            return (
                                                <Col xs={{ span: 24, order: index.order }} md={{ span: 12, order: index.order }} lg={{ span: 12, order: index.order }} className="ltr resfeatures">
                                                    <div className="resFImgOuter">
                                                        <img src={index.img} />
                                                    </div>
                                                    <h3>{index.title}</h3>
                                                    <p>{index.desc}</p>
                                                </Col>
                                            );
                                        })}
                                    </Row>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 18, order: 1 }} lg={{ span: 8, order: 2 }} className="resFetureImg">
                                    <img src={img4} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row type="flex" align="middle" className="UAF-container-fluid">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 20, order: 1 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_rp_section6_head1')}</h1>
                                        <p>{t('FacP_rp_section6_text1')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img5} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 20, order: 2 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} className="rtl maintextright">
                                    <div>
                                        <h1>{t('FacP_rp_section6_head2')}</h1>
                                        <p>{t('FacP_rp_section6_text2')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgleft">
                                    <img src={img6} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 20, order: 3 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_rp_section6_head3')}</h1>
                                        <p>{t('FacP_rp_section6_text3')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img7} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row type="flex" align="middle" className="resaddFeature">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 20, order: 1 }} type="flex" align="middle" className="UAF-container-fluid sectionHeading">
                            <h1>{t('FacP_rp_section7_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 19, order: 2 }} type="flex" align="middle" className="UAF-container-fluid resadFeatureColMain">
                            <Row type="flex" align="middle" gutter={16} className="ltr">
                                {addfeatures.map(index => {
                                    return (
                                        <Col xs={{ span: 24, order: index.order }} md={{ span: 8, order: index.order }} lg={{ span: 8, order: index.order }} className="ltr resadfeatures">
                                            <div className="resadFImgOuter">
                                                <img src={index.img} />
                                            </div>
                                            <h3>{index.title}</h3>
                                            <p>{index.desc}</p>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </Col>
                    </Row>
                    <Row type="flex" align="middle" className="UAF-container-fluid">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 20, order: 1 }} className="sectionHeading" style={{ paddingTop: "5rem" }}>
                            <h1 className="colordark">{t('FacP_rp_section7_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="sectionContent">
                            <SSsliderFacility screenshots={mstore_Screenshot} />
                        </Col>
                        <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 20, order: 3 }} className="sectionHeading">
                            <h1 className="colordark">{t('FacP_rp_section9_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 4 }} md={{ span: 24, order: 4 }} lg={{ span: 20, order: 4 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_rp_section9_head1')}</h1>
                                        <p>{t('FacP_rp_section9_text1')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img8} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 5 }} md={{ span: 24, order: 5 }} lg={{ span: 20, order: 5 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} className="rtl maintextright">
                                    <div>
                                        <h1>{t('FacP_rp_section9_head2')}</h1>
                                        <p>{t('FacP_rp_section9_text2')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgleft">
                                    <img src={img9} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 6 }} md={{ span: 24, order: 6 }} lg={{ span: 20, order: 6 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_rp_section9_head3')}</h1>
                                        <p>{t('FacP_rp_section9_text3')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img10} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 7 }} md={{ span: 24, order: 7 }} lg={{ span: 20, order: 7 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} className="rtl maintextright">
                                    <div>
                                        <h1>{t('FacP_rp_section9_head4')}</h1>
                                        <p>{t('FacP_rp_section9_text4')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgleft">
                                    <img src={img11} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </div>
        </React.Fragment>
    );
}

export default ResProducts;