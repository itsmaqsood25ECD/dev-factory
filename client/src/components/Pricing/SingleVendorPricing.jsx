/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor } from "antd";
import { Link, NavLink } from 'react-router-dom'
import CurrencyFormat from "react-currency-format";
import "../../assets/css/packagePrice.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { withTranslation } from 'react-i18next';
import i18next from 'i18next';
import tick from '../../assets/img/singleVendor/tick.svg'
import cross from '../../assets/img/singleVendor/cross.svg'

const { TabPane } = Tabs;
const { Title } = Typography;

class PackagePriceVerticleEcommerce extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //plan B
      WFPP: this.props.WFPP,
      WAFPP: this.props.WAFPP,
      MFPP: this.props.MFPP,

      // Plan A
      MBDSPM: this.props.MBDSPM,
      MADSPM: this.props.MADSPM,
      MBDSPY: this.props.MBDSPY,
      MADSPY: this.props.MADSPY,
      WBDSPM: this.props.WBDSPM,
      WADSPM: this.props.WADSPM,
      WBDSPY: this.props.WBDSPY,
      WADSPY: this.props.WADSPY,
      WABDSPM: this.props.WABDSPM,
      WAADSPM: this.props.WAADSPM,
      WABDSPY: this.props.WABDSPY,
      WAADSPY: this.props.WAADSPY,
      Duration: this.props.duration,
      currency: this.props.currency,
      MBDSP: this.props.MBDSPY,
      MADSP: this.props.MADSPY,
      WBDSP: this.props.WBDSPY,
      WADSP: this.props.WADSPY,
      WABDSP: this.props.WABDSPY,
      WAADSP: this.props.WAADSPY,
      PackType: this.props.PackType,
      SLUG: this.props.SLUG,
      ID: this.props.ID,

      // Setup Charge
      WSC: this.props.WSC,
      MSC: this.props.MSC,
      WMSC: this.props.WMSC,

      // User 
      USER: this.props.USER


    };
  }


  componentWillReceiveProps(nextApp) {
    this.setState({
      // Plan B
      WFPP: nextApp.WFPP,
      WAFPP: nextApp.WAFPP,
      MFPP: nextApp.MFPP,
      // Plan A
      MBDSPM: nextApp.MBDSPM,
      MADSPM: nextApp.MADSPM,
      MBDSPY: nextApp.MBDSPY,
      MADSPY: nextApp.MADSPY,
      WBDSPM: nextApp.WBDSPM,
      WADSPM: nextApp.WADSPM,
      WBDSPY: nextApp.WBDSPY,
      WADSPY: nextApp.WADSPY,
      WABDSPM: nextApp.WABDSPM,
      WAADSPM: nextApp.WAADSPM,
      WABDSPY: nextApp.WABDSPY,
      WAADSPY: nextApp.WAADSPY,
      Duration: nextApp.duration,
      currency: nextApp.currency,
      MBDSP: nextApp.MBDSPY,
      MADSP: nextApp.MADSPY,
      WBDSP: nextApp.WBDSPY,
      WADSP: nextApp.WADSPY,
      WABDSP: nextApp.WABDSPY,
      WAADSP: nextApp.WAADSPY,
      PackType: nextApp.PackType,
      SLUG: nextApp.SLUG,
      ID: nextApp.ID,
      // Setup Charge
      WSC: nextApp.WSC,
      MSC: nextApp.MSC,
      WMSC: nextApp.WMSC,
      USER: nextApp.USER,
    });
  }

  handleChange = e => {
    if (e.target.value === "Year") {
      this.setState({
        MBDSP: this.state.MBDSPY,
        MADSP: this.state.MADSPY,
        WBDSP: this.state.WBDSPY,
        WADSP: this.state.WADSPY,
        WABDSP: this.state.WABDSPY,
        WAADSP: this.state.WAADSPY,
        PackType: "Annually"
      });
    } else if (e.target.value === "Month") {
      this.setState({
        MBDSP: this.state.MBDSPM,
        MADSP: this.state.MADSPM,
        WBDSP: this.state.WBDSPM,
        WADSP: this.state.WADSPM,
        WABDSP: this.state.WABDSPM,
        WAADSP: this.state.WAADSPM,
        PackType: "Monthly"
      });
    }
  };

  render() {
    const currency = this.props.currency;
    const WSC = this.props.WSC;
    const WMSC = this.props.WMSC;
    const MSC = this.props.MSC;
    // const USER = this.props.USER;
    const { t } = this.props;

    return (

      <div className="package-bg" dir="ltr">
        {/* <RequestForDemo /> */}
        <Row
          gutter={16}
          type="flex"
          justify="center"
          align="middle"
          style={{ margin: "0px 0px", border: "0px solid #eee" }}
        >
          <Col lg={24} xs={24} className="">
            <div
              className="V-Price-Switcher"
              style={{
                textAlign: "center",
                marginTop: "15px",
                marginBottom: "15px"
              }}
            >

              <Radio.Group
                defaultValue="Year"
                buttonStyle="solid"
                onChange={this.handleChange}
                defaultValue="Year"
                size="large"
                className="Pack-chooser-container"
                shape="circle"
              >
                <Radio.Button className="year" value="Year">
                  {t('pricing_yearly')}
                </Radio.Button>
                <Radio.Button className="month" value="Month">
                  {t('pricing_Monthly')}
                </Radio.Button>
              </Radio.Group>
            </div>
          </Col>
          <Col xs={24}>
              <Row gutter={16}>
              <div style={{ margin: "60px auto", maxWidth: "1080px" }}>
                {/* Website Package */}
                <Col xs={24} md={8} lg={8}>
                  <div className="pack-card Basic">
                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        {t("pricing_basic")}
                      </Title>
                    </div>
                    <Title
                            level={2}
                            class="light-text"
                            style={{
                              marginTop: "10px",
                              marginBottom: "30px",
                              textAlign: "center"
                            }}
                          >
                            <div className="before-discount-price">
                              {currency}&nbsp;
                  <CurrencyFormat
                                value={Number(this.state.WBDSP)}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                            </div>
                &nbsp;
                <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat
                                value={Number(this.state.WADSP)}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          
                    <div
                      style={{
                       
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                       <div
                        style={{
                          maxWidth: "230px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                       <p><img src={cross} style={{width:"25px",height:"25px"}} alt="cross" /> {t("pricing_androidIos")} </p>
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        <p><img src={tick} style={{width:"25px",height:"25px"}} alt="tick" /> {t("pricing_ecommWebsite")}</p> 
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        <p><img src={tick} style={{width:"25px",height:"25px"}} alt="tick" /> {t("pricing_allFeatures")} </p> 
                      </div>

                      <div></div>
                    </div>
                    <div>
                          <div style={{ textAlign: "center" }}>
                            {this.state.USER != null ? <Link
                              to="/eCommerce-Single-Vendor-Platform/Basic/Standard"
                            >
                              <Button className="SubscribeNow-btn" type="primary">Subscribe Now</Button>
                            </Link> : <Link
                              to="/login"
                            >
                                <Button className="SubscribeNow-btn" type="primary">Subscribe Now</Button>
                              </Link>
                            }
                            <div className="one-time-setup-tag">
                              *{t("pricing_oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat
                                value={WSC}
                                displayType={"text"}
                                thousandSeparator={true}
                              /> {" "} {t("pricing_willApply")}
                            </div>
                          </div>
                        </div>
                  </div>
                </Col>
                {/* End Website Package */}
                {/* Web App Package */}
                <Col xs={24} md={8} lg={8}>
                  <div
                    className="pack-card ultra"
                    style={{ marginTop: "-10px" }}
                  >
                    <div className="recommended-tag">
                      recommended
                    </div>
                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        {" "}
                        {t("pricing_ULTRA")}
                      </Title>
                    </div>
                    <Title level={2} class="light-text" style={{ marginTop: "10px", marginBottom: "30px", textAlign: "center" }} >
                            <div className="before-discount-price"> {currency}&nbsp;
                              <CurrencyFormat value={Number(this.state.WABDSP)} displayType={"text"} thousandSeparator={true} />
                            </div>&nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.WAADSP)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          
                          <div
                      style={{
                       
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                       <div
                        style={{
                          maxWidth: "230px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                       <p><img src={tick} style={{width:"25px",height:"25px"}} alt="cross" /> {t("pricing_androidIos")} </p>
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        <p><img src={tick} style={{width:"25px",height:"25px"}} alt="tick" /> {t("pricing_ecommWebsite")}</p> 
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        <p><img src={tick} style={{width:"25px",height:"25px"}} alt="tick" /> {t("pricing_allFeatures")} </p> 
                      </div>

                      <div></div>
                    </div>
                    
                    {/* New Tags */}

                    <div>
                          <div style={{ textAlign: "center" }}>
                            {/* {console.log(this.state.USER)} */}
                            {this.state.USER != null ? <Link to="/eCommerce-Single-Vendor-Platform/Ultra/Standard" >
                              <Button className="SubscribeNow-btn" type="primary">Subscribe Now</Button>
                            </Link> : <Link to="/login" >
                                <Button className="SubscribeNow-btn" type="primary">Subscribe Now</Button>
                              </Link>
                            }
                            <div className="one-time-setup-tag">
                              *{t("pricing_oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat value={WMSC} displayType={"text"} thousandSeparator={true} /> {" "} {t("pricing_willApply")}
                            </div>
                          </div>
                        </div>
                    {/* New Tags end */}
                  </div>
                </Col>
                {/* End Web App Package */}
                {/* Mobile App Package */}
                <Col xs={24} md={8} lg={8}>
                  <div className="pack-card Premium">
                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        {t("pricing_PREMIUM")}
                      </Title>
                    </div>

                    <Title level={2} class="light-text" style={{ marginTop: "10px", marginBottom: "30px", textAlign: "center" }} >
                            <div className="before-discount-price"> {currency}&nbsp;
                              <CurrencyFormat value={Number(this.state.MBDSP)} displayType={"text"} thousandSeparator={true} />
                            </div> &nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.MADSP)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                    </Title>
                          
                    <div
                      style={{
                       
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                       <div
                        style={{
                          maxWidth: "230px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                       <p><img src={tick} style={{width:"25px",height:"25px"}} alt="tick" /> {t("pricing_androidIos")} </p>
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        <p><img src={cross} style={{width:"25px",height:"25px"}} alt="cross" /> {t("pricing_ecommWebsite")}</p> 
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        <p><img src={tick} style={{width:"25px",height:"25px"}} alt="tick" /> {t("pricing_allFeatures")} </p> 
                      </div>

                      <div></div>
                    </div>
                    
                    {/* New Tags */}

                    <div>
                          <div style={{ textAlign: "center" }}> {this.state.USER != null ? <Link to="/eCommerce-Single-Vendor-Platform/Premium/Standard" >
                            <Button className="SubscribeNow-btn" type="primary">Subscribe Now</Button>
                          </Link> : <Link to="/login" >
                              <Button className="SubscribeNow-btn" type="primary">Subscribe Now</Button>
                            </Link>}
                            <div className="one-time-setup-tag"> *{t("pricing_oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat value={MSC} displayType={"text"} thousandSeparator={true} /> {" "}
                              {t("pricing_willApply")}
                            </div>
                          </div>
                        </div>
                    {/* New Tags end */}
                  </div>
                </Col>
                {/* End Mobile App Package */}
              </div>
            </Row>

          </Col>
        </Row>
      </div>
    );
  }
}
PackagePriceVerticleEcommerce.propTypes = {
  ...withDirectionPropTypes,
};

export default withTranslation()(withDirection(PackagePriceVerticleEcommerce));
