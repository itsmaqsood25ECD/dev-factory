import React from "react";
import { Row, Col, Button, Card, Icon } from "antd";
import { RightOutlined, MinusOutlined } from '@ant-design/icons'
import SSsliderFacility from "../pages/SSsliderFacility";
import '../assets/css/product.css'
import '../assets/css/mobilescreenSlider.css'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import { NavLink } from "react-router-dom";
import { useTranslation } from 'react-i18next';
import img1 from '../assets/img/multivendor/img1.png'
import img2 from '../assets/img/multivendor/img2.png'
import img3 from '../assets/img/multivendor/img3.png'
import img4 from '../assets/img/multivendor/img4.png'
import img5 from '../assets/img/multivendor/img5.png'
import img6 from '../assets/img/multivendor/img6.png'
import img7 from '../assets/img/multivendor/img7.png'
import tick from '../assets/img/multivendor/tick.png'
import fimg1 from '../assets/img/multivendor/fimg1.png'
import fimg2 from '../assets/img/multivendor/fimg2.png'
import fimg3 from '../assets/img/multivendor/fimg3.png'
import fimg4 from '../assets/img/multivendor/fimg4.png'
import fimg5 from '../assets/img/multivendor/fimg5.png'
import fimg6 from '../assets/img/multivendor/fimg6.png'
import fimg7 from '../assets/img/multivendor/fimg7.png'
import fimg8 from '../assets/img/multivendor/fimg8.png'
import fimg9 from '../assets/img/multivendor/fimg9.png'
import fimg10 from '../assets/img/multivendor/fimg10.png'
import fimg11 from '../assets/img/multivendor/fimg11.png'
import fimg12 from '../assets/img/multivendor/fimg12.png'
import scr1 from '../assets/img/multivendor/scr1.png'
import scr2 from '../assets/img/multivendor/scr2.png'
import scr3 from '../assets/img/multivendor/scr3.png'
import scr4 from '../assets/img/multivendor/scr4.png'
import scr5 from '../assets/img/multivendor/scr5.png'



const MVProducts = () => {

    const { t } = useTranslation();

    const mvfeatures = [
        {
            order: '1',
            img: fimg1,
            title: `${t("FacP_mvp_section3_f1")}`
        },
        {
            order: '2',
            img: fimg2,
            title: `${t("FacP_mvp_section3_f2")}`
        },
        {
            order: '3',
            img: fimg3,
            title: `${t("FacP_mvp_section3_f3")}`
        },
        {
            order: '4',
            img: fimg4,
            title: `${t("FacP_mvp_section3_f4")}`
        },
        {
            order: '5',
            img: fimg5,
            title: `${t("FacP_mvp_section3_f5")}`
        },
        {
            order: '6',
            img: fimg6,
            title: `${t("FacP_mvp_section3_f6")}`
        },
        {
            order: '7',
            img: fimg7,
            title: `${t("FacP_mvp_section3_f7")}`
        },
        {
            order: '8',
            img: fimg8,
            title: `${t("FacP_mvp_section3_f8")}`
        },
        {
            order: '9',
            img: fimg9,
            title: `${t("FacP_mvp_section3_f9")}`
        },
        {
            order: '10',
            img: fimg10,
            title: `${t("FacP_mvp_section3_f10")}`
        },
        {
            order: '11',
            img: fimg11,
            title: `${t("FacP_mvp_section3_f11")}`
        },
        {
            order: '12',
            img: fimg12,
            title: `${t("FacP_mvp_section3_f12")}`
        }
    ]

    const mstore_Screenshot = [
        {
            img: scr1,
            // name: "Home Page"
        },
        {
            img: scr2,
            // name: "Categories Page"
        },
        {
            img: scr3,
            // name: "Categories list View"
        },
        {
            img: scr4,
            // name: "Sub Categories"
        },
        {
            img: scr5,
            // name: "Search Page"
        },
        {
            img: scr1,
            // name: "Home Page"
        },
        {
            img: scr2,
            // name: "Categories Page"
        },
        {
            img: scr3,
            // name: "Categories list View"
        }
    ];
    return (
        <React.Fragment>
            <div className="UAF-container-fluid">
                <div className="mvp_Main">
                    <Row type="flex" align="middle">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 20, order: 1 }} className="sectionHeading">
                            <h1>{t('FacP_mvp_section2_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 20, order: 2 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_mvp_section2_block1_head')}</h1>
                                        <p>{t('FacP_mvp_section2_block1_text')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img1} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 20, order: 3 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} className="rtl maintextright">
                                    <div>
                                        <h1>{t('FacP_mvp_section2_block2_head')}</h1>
                                        <p>{t('FacP_mvp_section2_block2_text')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgleft">
                                    <img src={img2} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 4 }} md={{ span: 24, order: 4 }} lg={{ span: 20, order: 4 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_mvp_section2_block3_head')}</h1>
                                        <p>{t('FacP_mvp_section2_block3_text')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img3} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 5 }} md={{ span: 24, order: 5 }} lg={{ span: 20, order: 5 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} className="rtl maintextright">
                                    <div>
                                        <h1>{t('FacP_mvp_section2_block4_head')}</h1>
                                        <p>{t('FacP_mvp_section2_block4_text')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgleft">
                                    <img src={img4} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 6 }} md={{ span: 24, order: 6 }} lg={{ span: 20, order: 6 }} className="sectionHeading">
                            <h1>{t('FacP_mvp_section2_block5_head')}</h1>
                            <hr />
                            <p>{t('FacP_mvp_section2_block5_text')}</p>
                        </Col>
                        <Col xs={{ span: 24, order: 7 }} md={{ span: 24, order: 7 }} lg={{ span: 18, order: 7 }} className="sectionContent imgCol">
                            <div>
                                <img src={img5} />
                            </div>
                        </Col>
                        <Col xs={{ span: 24, order: 8 }} md={{ span: 24, order: 8 }} lg={{ span: 20, order: 8 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="ltr maintextLeft">
                                    <div>
                                        <h1>{t('FacP_mvp_section2_block6_head')}</h1>
                                        <p>{t('FacP_mvp_section2_block6_text')}</p>
                                        <ul className="sectioncontentUl">
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section2_block6_li1')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section2_block6_li2')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section2_block6_li3')}</li>
                                        </ul>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgright">
                                    <img src={img6} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 9 }} md={{ span: 24, order: 9 }} lg={{ span: 20, order: 9 }} className="sectionContent">
                            <Row type="flex" align="middle">
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} className="rtl maintextright">
                                    <div>
                                        <h1>{t('FacP_mvp_section2_block7_head')}</h1>
                                        <p>{t('FacP_mvp_section2_block7_text')}</p>
                                        <a href="#uaf_cp_id_ContactUs">{t('FacP_mvp_get_started')}</a>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="mainimgleft">
                                    <img src={img7} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 10 }} md={{ span: 24, order: 10 }} lg={{ span: 20, order: 10 }} className="sectionHeading">
                            <h1>{t('FacP_mvp_section3_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 11 }} md={{ span: 24, order: 11 }} lg={{ span: 20, order: 11 }} className="sectionContent">
                            <Row type="flex" align="middle" gutter={16}>
                                {mvfeatures.map(index => {
                                    return (
                                        <Col xs={{ span: 12, order: index.order }} md={{ span: 6, order: index.order }} lg={{ span: 4, order: index.order }} className="features">
                                            <div>
                                                <img src={index.img} />
                                            </div>
                                            <h3>{index.title}</h3>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 12 }} md={{ span: 24, order: 12 }} lg={{ span: 20, order: 12 }} className="sectionHeading">
                            <h1>{t('FacP_mvp_section4_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 13 }} md={{ span: 24, order: 13 }} lg={{ span: 24, order: 13 }} className="sectionContent">
                            <SSsliderFacility screenshots={mstore_Screenshot} />
                        </Col>
                        <Col xs={{ span: 24, order: 14 }} md={{ span: 24, order: 14 }} lg={{ span: 20, order: 14 }} className="sectionHeading">
                            <h1>{t('FacP_mvp_section5_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 14 }} md={{ span: 22, order: 14 }} lg={{ span: 16, order: 14 }} className="sectionContent">
                            <Row type="flex" align="middle" className="pricingOuter">
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 10, order: 1 }} className="ltr maintextLeft darkbg">
                                    <div>
                                        <ul className="sectioncontentUl">
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li1')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li2')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li3')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li4')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li5')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li6')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li7')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li8')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li9')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li10')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li11')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li12')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li13')}</li>
                                            <li><img src={tick} className="tick" /> {t('FacP_mvp_section5_li14')}</li>
                                        </ul>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 14, order: 2 }} className="pricecontentRight">
                                    <div>
                                        <p>{t('FacP_mvp_section5_get_all')}</p>
                                        <p>usd <span>4,900</span></p>
                                        <a>{t('FacP_contact_head')}</a>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </div>
        </React.Fragment>
    );
}

export default MVProducts;