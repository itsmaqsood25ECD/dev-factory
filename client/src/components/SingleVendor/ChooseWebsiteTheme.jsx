/* eslint react/prop-types: 0 */
import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Button,
  Icon
} from "antd";

// import ScrollTop from "../../ScrollTop";
import { Link, Redirect } from "react-router-dom";
import WebsiteThemes from "../WebsiteThemes";
import MobileAppView from "../MobileAppView/MobileAppPage";
import { Helmet } from "react-helmet";

import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

const ChooseWebsiteTheme = ({
  direction,
  user,
  match
}) => {

  const { t } = useTranslation();


  useEffect(() => {
  
    console.log("match >>>", match.params);

  }, []);

 
  if (user === null) {
    return <Redirect to="/login"></Redirect>;
  } return(
      <div>
        {/* <ScrollTop /> */}
        <Helmet>
          <title>UPapp factory - Buy Ready Made Apps</title>
          <meta
            name="Readymade eCommerce Mobile Apps for Android and iOS Platforms | UPapp factory"
            content="Build native mobile ecommerce app (android & iOS) for your eCommerce store. Readymade ecommerce app on all platforms. we help you by developing a feature-rich mobile app solution depends on your business requirements."
          />
        </Helmet>
     
        
        {match.params.Package !=  "Premium" ? (
        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            textAlign: "center",
            margin: "150px auto 0px auto"
          }}
        >
          <div
            style={{
              margin: "55px auto"
            }}
            className="single-app-heading-text"
          >
            {t("ecommercePage_ChhoseDemo")}
          </div>
          <WebsiteThemes 
          Package={match.params.Package}
          Plan={match.params.Plan}
           />
        </section>
        ): null }
{match.params.Package !=  "Basic" ? (
  <Fragment>
  {match.params.Package ===  "Premium" ? (  
    <div style={{border:"2px Solid #000",borderLeft:"none",borderRight:"none",background:"#000",marginTop:"85px",height:"50px",padding:"5px"}}>
{/* <Link to={`/eCommerce-Single-Vendor-Platform/${match.params.Package}/${match.params.Plan}`}>
<Button type="primary" style={{background:'none',border:'none',float:"left",fontSize:"16px",color:"#d9d9d9"}}>
<Icon type="left" />
            Back To Theme Selection
  </Button>
  </Link> */}
<Link to={`/eCommerce-Single-Vendor-Platform/${match.params.Package}/${match.params.Plan}/APP-01/Checkout`}>
<Button type="primary" style={{background:'none',border:'none',float:"right",fontSize:"16px",color:"#d9d9d9"}}>
{t("ecommercePage_proceed")}
            <Icon type="right" />
  </Button>
  </Link>
</div>
  ):null}
<section
        className="UAF-container-fluid"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          textAlign: "center",
          margin: "50px auto 50px auto"
        }}
      >
        <div className="single-app-heading-text" style={{marginBottom:"50px"}}>
              <br />
              {t("singleapp_HowYourAppWillLookToYourCustomers")}
            </div>

        <MobileAppView />
      </section>

  </Fragment>
      
) : null}


</div>
    );
};



ChooseWebsiteTheme.propTypes = {
  user: PropTypes.object.isRequired,
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({
  user: state.auth.user
});

export default connect(mapStateToProps, {
 
})(withDirection(ChooseWebsiteTheme));
