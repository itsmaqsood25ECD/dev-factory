
import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Result, Button } from 'antd';


export default class CallBackConfirmation extends Component {
	constructor(props) {
		super(props);
		this.orderString = 'something went wrong... please try again';
		this.title = `Payment Failed`;
	}
	render() {
		return (
			<Fragment>
				{window.scrollTo(0, 0)}
				<Result
					style={{ paddingTop: '150px', height:"100vH" }}
					status="error"
					title={this.title}
					subTitle={this.orderString}
					extra={[
						<Link to="/">
							<Button type="primary" key="console">
								Go to Home Page
							</Button>
						</Link>
					]}
				/>
			</Fragment>
		);
	}
}
