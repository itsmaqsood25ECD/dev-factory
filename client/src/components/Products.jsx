import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Row, Col, Button, Card, Icon } from "antd";
import { RightOutlined, MinusOutlined } from '@ant-design/icons'
import { getRecentBlog } from "../actions/admin/blog.management.action";
import moment from 'moment'
import { useTranslation } from 'react-i18next';
import '../assets/css/product.css'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import { NavLink, Link } from "react-router-dom";
import btimage1 from '../assets/img/single-vendor-img.png'
import btimage2 from '../assets/img/multi-vendor-img.png'
import btimage3 from '../assets/img/restaurant.png'
import device1 from '../assets/img/device1.png'
import device2 from '../assets/img/device2.png'
import device3 from '../assets/img/device3.png'
import img2 from '../assets/img/specialimg.png'
import res1 from '../assets/img/res1.png'
import res2 from '../assets/img/res2.png'
import res3 from '../assets/img/res3.png'
import res4 from '../assets/img/res4.png'
import bindera from '../assets/img/bindera.png'
import proj2 from '../assets/img/proj2.png'
import proj3 from '../assets/img/proj3.png'
import proj4 from '../assets/img/proj4.png'
import proj5 from '../assets/img/proj5.png'
import proj6 from '../assets/img/proj6.png'
import blog1 from '../assets/img/blog1.png'
import blog2 from '../assets/img/blog2.png'
import blog3 from '../assets/img/blog3.png'
const { Meta } = Card;

var blogSliderSettings = {
    dots: false,
    arrows: false,
    centerMode: false,
    speed: 200,
    swipeToSlide: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    lazyLoad: true,
    initialSlide: 0,

    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                dots: false
            }
        },
        {
            breakpoint: 770,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                initialSlide: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};
var clientSliderSettings = {
    dots: false,
    arrows: false,
    centerMode: true,
    speed: 200,
    swipeToSlide: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    lazyLoad: true,
    initialSlide: 0,

    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: false
            }
        },
        {
            breakpoint: 770,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                initialSlide: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
};
const BusinessOnline = ({ getRecentBlog, recentBlog, }) => {

    useEffect(() => {
        getRecentBlog()
        console.log("recentBlog", recentBlog)
    }, [])

    const { t } = useTranslation();

    const businessType = [
        {
            order: '1',
            img: btimage1,
            title: `${t("FacP_home_section3_single_title_line1")}`,
            title2: `${t("FacP_home_section3_single_title_line2")}`,
            desc: `${t("FacP_home_section3_single_text")}`,
            reflink: '/eCommerce-Single-Vendor-Platform'
        },
        {
            order: '2',
            img: btimage2,
            title: `${t("FacP_home_section3_multi_title_line1")}`,
            title2: `${t("FacP_home_section3_multi_title_line2")}`,
            desc: `${t("FacP_home_section3_multi_text")}`,
            reflink: '/eCommerce-Multi-Vendor-Platform'
        },
        {
            order: '3',
            img: btimage3,
            title: `${t("FacP_home_section3_rest_title_line1")}`,
            title2: `${t("FacP_home_section3_rest_title_line2")}`,
            desc: `${t("FacP_home_section3_rest_text")}`,
            reflink: '/restaurant'
        },
        // {
        //     order: '4',
        //     img: btimage3,
        //     title: 'UPapp Chatbot',
        //     reflink: ''
        // },
    ]
    const reasonToGrow = [
        {
            order: '1',
            img: res1,
            title: `${t("FacP_home_section6_card1_head")}`,
            desc: `${t("FacP_home_section6_card1_text")}`
        },
        {
            order: '2',
            img: res2,
            title: `${t("FacP_home_section6_card2_head")}`,
            desc: `${t("FacP_home_section6_card2_text")}`
        },
        {
            order: '3',
            img: res3,
            title: `${t("FacP_home_section6_card3_head")}`,
            desc: `${t("FacP_home_section6_card3_text")}`
        },
        {
            order: '4',
            img: res4,
            title: `${t("FacP_home_section6_card4_head")}`,
            desc: `${t("FacP_home_section6_card4_text")}`
        },
    ]
    return (
        <React.Fragment>
            <div className="UAF-container-fluid">
                <div className="businessonlineMain">
                    <Row type="flex" align="middle">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                            <h1>{t('FacP_home_section3_head')}</h1>
                            <hr />
                            <p>{t('FacP_home_section3_text')}</p>
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="sectionContent">
                            <Row type="flex" align="middle" gutter={16}>
                                {businessType.map(index => {
                                    return (
                                        <Col xs={{ span: 24, order: index.order }} md={{ span: 18, order: index.order }} lg={{ span: 8, order: index.order }} className="btBlockTile">
                                            <NavLink to={index.reflink}>
                                                <div className="blockImage">
                                                    <img src={index.img} />
                                                </div>
                                                <div className="blockData">
                                                    <h1>{index.title}<br />{index.title2}</h1>
                                                    <p>{index.desc}</p>
                                                    <a href={index.reflink}><Button className="btn Start-Now-btn" >{t('FacP_home_explore')}</Button></a>
                                                    {/* <a href={index.reflink}>Learn more <RightOutlined /></a> */}
                                                </div>
                                            </NavLink>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="businessOnlineFullSection1">
                <Row type="flex" align="middle">
                    <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                        <h1>{t('FacP_home_section4_head')}</h1>
                        <hr />
                    </Col>
                    <Col xs={{ span: 24, order: 2 }} md={{ span: 20, order: 2 }} lg={{ span: 20, order: 2 }} className="sectionContent">
                        <Row type="flex" align="middle" className="imageRow" gutter={15}>
                            <Col xs={{ span: 24, order: 1 }} md={{ span: 8, order: 1 }} lg={{ span: 8, order: 1 }} className="deviceImg">
                                <img src={device1} />
                            </Col>
                            <Col xs={{ span: 24, order: 2 }} md={{ span: 8, order: 2 }} lg={{ span: 8, order: 2 }} className="deviceImg">
                                <img src={device2} />
                            </Col>
                            <Col xs={{ span: 24, order: 3 }} md={{ span: 8, order: 3 }} lg={{ span: 8, order: 3 }} className="deviceImg">
                                <img src={device3} />
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
            <div className="businessOnlineFullSection2">
                <Row type="flex" align="middle">
                    <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                        <h1>{t('FacP_home_section5_head')}</h1>
                        <hr />
                    </Col>
                    <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="sectionContent">
                        <Row type="flex" className="bofs2_mainRow">
                            <Col xs={{ span: 24, order: 1 }} md={{ span: 13, order: 1 }} lg={{ span: 13, order: 1 }} className="bofs2_mainCol">
                                <img src={img2} />
                            </Col>
                            <Col xs={{ span: 24, order: 2 }} md={{ span: 11, order: 2 }} lg={{ span: 11, order: 2 }} className="bofs2_mainCol">
                                <Row type="flex" className="bofs2_mainRow">
                                    <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 20, order: 1 }} className="bofs2_mainTextCol">
                                        <h1>{t('FacP_home_section5_cont1_head')}</h1>
                                        <p>{t('FacP_home_section5_cont1_text')}</p>
                                    </Col>
                                    <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 20, order: 2 }} className="bofs2_mainTextCol">
                                        <h1>{t('FacP_home_section5_cont2_head')}</h1>
                                        <p>{t('FacP_home_section5_cont2_text')}</p>
                                    </Col>
                                    <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 20, order: 3 }} className="bofs2_mainTextCol">
                                        <h1>{t('FacP_home_section5_cont3_head')}</h1>
                                        <p>{t('FacP_home_section5_cont3_text')}</p>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
            <div className="businessOnlineFullSection3">
                <div className="UAF-container-fluid">
                    <Row type="flex" align="middle">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                            <h1>{t('FacP_home_section6_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="sectionContent">
                            <Row type="flex" className="bofs3_mainRow" gutter={16}>
                                {reasonToGrow.map(index => {
                                    return (
                                        <Col xs={{ span: 24, order: index.order }} md={{ span: 12, order: index.order }} lg={{ span: 6, order: index.order }} className="bofs3_Col">
                                            <div className="bofs3_img">
                                                <img src={index.img} />
                                            </div>
                                            <h1>{index.title}</h1>
                                            <p>{index.desc}</p>
                                        </Col>
                                    );
                                })}
                            </Row>
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="businessOnlineFullSection4">
                <div className="UAF-container-fluid">
                    <Row type="flex" align="middle">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                            <h1>{t('FacP_home_section7_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="sectionContent">
                            <Row type="flex" className="bofs4_mainRow">
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 8, order: 1 }} className="bofs4_Col">
                                    <div className="bosf_Tile bosf_TileBg1">
                                        <div className="bofs4_img">
                                            <img src={proj3} />
                                        </div>
                                        <h1>Mani's Cafe</h1>
                                        <p>{t('FacP_home_section7_card4_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 8, order: 2 }} className="bofs4_Col">
                                    <div className="bosf_Tile bosf_TileBg2">
                                        <div className="bofs4_img">
                                            <img src={bindera} />
                                        </div>
                                        <h1>Bindera</h1>
                                        <p>{t('FacP_home_section7_card2_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 3 }} md={{ span: 12, order: 3 }} lg={{ span: 8, order: 3 }} className="bofs4_Col">
                                    <div className="bosf_Tile bosf_TileBg3">
                                    <div className="bofs4_img">
                                            <img src={proj6} />
                                        </div>
                                        <h1>FARMASI</h1>
                                        <p>{t('FacP_home_section7_card6_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 4 }} md={{ span: 12, order: 4 }} lg={{ span: 8, order: 4 }} className="bofs4_Col">
                                    <div className="bosf_Tile bosf_TileBg4">
                                        <div className="bofs4_img">
                                            <img src={proj4} />
                                        </div>
                                        <h1>{t('FacP_home_section7_card4_head')}</h1>
                                        <p>{t('FacP_home_section7_card4_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 5 }} md={{ span: 12, order: 5 }} lg={{ span: 8, order: 5 }} className="bofs4_Col">
                                    <div className="bosf_Tile bosf_TileBg5">
                                        <div className="bofs4_img">
                                            <img src={proj5} />
                                        </div>
                                        <h1>Burger Garage</h1>
                                        <p>{t('FacP_home_section7_card5_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 6 }} md={{ span: 12, order: 6 }} lg={{ span: 8, order: 6 }} className="bofs4_Col">
                                    <div className="bosf_Tile bosf_TileBg6">
                                      <div className="bofs4_img">
                                            <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/mom/mom2.png" />
                                        </div>
                                        <h1>Smart MOM</h1>
                                        <p>{t('FacP_home_section7_card1_text')}</p>
                                    
                                     </div>
                                </Col>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 24, order: 3 }} className="sectionContent viewMoreBtn">
                            <Link to="/clientele">{t('FacP_home_view')} <RightOutlined /></Link>
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="businessOnlineFullSection5">
                <div className="UAF-container-fluid">
                    <Row type="flex" align="middle">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                            <h1>{t('FacP_home_section8_head')}</h1>
                            <hr />
                            <p>{t('FacP_home_section8_text')}</p>
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="sectionContent">
                            <Row type="flex" className="bofs5_mainRow">
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 8, order: 1 }} className="bofs5_Col">
                                    <div className="bosf5_Tile bosf5_TileBg1">
                                        <h1>{t('FacP_home_section8_card1_head')}</h1>
                                        <p>{t('FacP_home_section8_card1_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 8, order: 2 }} className="bofs5_Col">
                                    <div className="bosf5_Tile bosf5_TileBg2">
                                        <h1>{t('FacP_home_section8_card2_head')}</h1>
                                        <p>{t('FacP_home_section8_card2_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 3 }} md={{ span: 12, order: 3 }} lg={{ span: 8, order: 3 }} className="bofs5_Col">
                                    <div className="bosf5_Tile bosf5_TileBg3">
                                        <h1>{t('FacP_home_section8_card3_head')}</h1>
                                        <p>{t('FacP_home_section8_card3_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 4 }} md={{ span: 12, order: 4 }} lg={{ span: 8, order: 4 }} className="bofs5_Col">
                                    <div className="bosf5_Tile bosf5_TileBg4">
                                        <h1>{t('FacP_home_section8_card4_head')}</h1>
                                        <p>{t('FacP_home_section8_card4_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 5 }} md={{ span: 12, order: 5 }} lg={{ span: 8, order: 5 }} className="bofs5_Col">
                                    <div className="bosf5_Tile bosf5_TileBg5">
                                        <h1>{t('FacP_home_section8_card5_head')}</h1>
                                        <p>{t('FacP_home_section8_card5_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 6 }} md={{ span: 12, order: 6 }} lg={{ span: 8, order: 6 }} className="bofs5_Col">
                                    <div className="bosf5_Tile bosf5_TileBg6">
                                        <h1>{t('FacP_home_section8_card6_head')}</h1>
                                        <p>{t('FacP_home_section8_card6_text')}</p>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="businessOnlineFullSection6">
                <div className="UAF-container-fluid">
                    <Row type="flex" align="middle">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                            <h1>{t('FacP_home_section9_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 20, order: 2 }} className="sectionContent">
                            <Row type="flex">
                                <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} className="bofs6_Col">
                                    <div className="bosf6_Tile">
                                        <h1>{t('FacP_home_section9_cont1_head')}</h1>
                                        <p>{t('FacP_home_section9_cont1_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} className="bofs6_Col">
                                    <div className="bosf6_Tile">
                                        <h1>{t('FacP_home_section9_cont2_head1')}<br />{t('FacP_home_section9_cont2_head2')}</h1>
                                        <p>{t('FacP_home_section9_cont2_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 3 }} md={{ span: 12, order: 3 }} lg={{ span: 12, order: 3 }} className="bofs6_Col">
                                    <div className="bosf6_Tile">
                                        <h1>{t('FacP_home_section9_cont3_head')}</h1>
                                        <p>{t('FacP_home_section9_cont3_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 4 }} md={{ span: 12, order: 4 }} lg={{ span: 12, order: 4 }} className="bofs6_Col">
                                    <div className="bosf6_Tile">
                                        <h1>{t('FacP_home_section9_cont4_head')}</h1>
                                        <p>{t('FacP_home_section9_cont4_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 5 }} md={{ span: 12, order: 5 }} lg={{ span: 12, order: 5 }} className="bofs6_Col">
                                    <div className="bosf6_Tile">
                                        <h1>{t('FacP_home_section9_cont5_head1')}<br />{t('FacP_home_section9_cont5_head2')}</h1>
                                        <p>{t('FacP_home_section9_cont5_text')}</p>
                                    </div>
                                </Col>
                                <Col xs={{ span: 24, order: 6 }} md={{ span: 12, order: 6 }} lg={{ span: 12, order: 6 }} className="bofs6_Col">
                                    <div className="bosf6_Tile">
                                        <h1>{t('FacP_home_section9_cont6_head')}</h1>
                                        <p>{t('FacP_home_section9_cont6_text')}</p>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </div>

            {/* <div className="businessOnlineFullSection7">
                <div className="businessOnlineFullSection7Inner">
                    <Row gutter={16} type="flex" justify="space-around" align="middle">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                            <h1>{t('FacP_home_section10_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} sm={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} xl={{ span: 24, order: 2 }}>
                            <Row gutter={30}>
                                <Slider {...clientSliderSettings}>
                                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }} className="s7ClientCardOuter">
                                        <Row className="s7ClientCardInner clientbgpink">
                                            <Col xs={{ span: 10, order: 1 }} md={{ span: 10, order: 1 }} lg={{ span: 10, order: 1 }} className="s7CardLogo">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Fursahh_Logo_Final.png" />
                                            </Col>
                                            <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="s7CardContent">
                                                <h1>{t('FacP_home_section10_card_head')}</h1>
                                                <p>{t('FacP_home_section10_card_text')}</p>
                                                <p className="ClientnameDate">JOHN DOE <MinusOutlined /><span> July 10 2020</span></p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }} className="s7ClientCardOuter">
                                        <Row className="s7ClientCardInner clientbgblue">
                                            <Col xs={{ span: 10, order: 1 }} md={{ span: 10, order: 1 }} lg={{ span: 10, order: 1 }} className="s7CardLogo">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/ScanZone.png" />
                                            </Col>
                                            <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="s7CardContent">
                                                <h1>{t('FacP_home_section10_card_head')}</h1>
                                                <p>{t('FacP_home_section10_card_text')}</p>
                                                <p className="ClientnameDate">JOHN DOE <MinusOutlined /><span> July 10 2020</span></p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }} className="s7ClientCardOuter">
                                        <Row className="s7ClientCardInner clientbggreen">
                                            <Col xs={{ span: 10, order: 1 }} md={{ span: 10, order: 1 }} lg={{ span: 10, order: 1 }} className="s7CardLogo">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Al-Hilaal-final-logo.png" />
                                            </Col>
                                            <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="s7CardContent">
                                                <h1>{t('FacP_home_section10_card_head')}</h1>
                                                <p>{t('FacP_home_section10_card_text')}</p>
                                                <p className="ClientnameDate">JOHN DOE <MinusOutlined /><span> July 10 2020</span></p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }} className="s7ClientCardOuter">
                                        <Row className="s7ClientCardInner clientbggray">
                                            <Col xs={{ span: 10, order: 1 }} md={{ span: 10, order: 1 }} lg={{ span: 10, order: 1 }} className="s7CardLogo">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/crowneplaza.png" />
                                            </Col>
                                            <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="s7CardContent">
                                                <h1>{t('FacP_home_section10_card_head')}</h1>
                                                <p>{t('FacP_home_section10_card_text')}</p>
                                                <p className="ClientnameDate">JOHN DOE <MinusOutlined /><span> July 10 2020</span></p>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }} className="s7ClientCardOuter">
                                        <Row className="s7ClientCardInner clientbgyellow">
                                            <Col xs={{ span: 10, order: 1 }} md={{ span: 10, order: 1 }} lg={{ span: 10, order: 1 }} className="s7CardLogo">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Clientele/mobile-screens/logos/Comex+3%402x.png" />
                                            </Col>
                                            <Col xs={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} className="s7CardContent">
                                                <h1>{t('FacP_home_section10_card_head')}</h1>
                                                <p>{t('FacP_home_section10_card_text')}</p>
                                                <p className="ClientnameDate">JOHN DOE <MinusOutlined /><span> July 10 2020</span></p>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Slider>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 24, order: 3 }} className="sectionContent viewMoreBtn">
                            <a href="https://upappfactory.com/clientele" target="_blank">{t('FacP_home_view')} <RightOutlined /></a>
                        </Col>
                    </Row>
                </div>
            </div>
             */}
            <div className="businessOnlineFullSection8">
                <div className="UAF-container-fluid">
                    <Row gutter={16} type="flex" justify="space-around" align="middle">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} lg={{ span: 24, order: 1 }} className="sectionHeading">
                            <h1>{t('FacP_home_section11_card_head')}</h1>
                            <hr />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} sm={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} lg={{ span: 24, order: 2 }} xl={{ span: 24, order: 2 }}>
                            <Row gutter={30}>
                                <Slider {...blogSliderSettings} style={{ padding: "20px 50px" }}>
                                    {recentBlog && recentBlog.map((blog) => {
                                        return <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                                            <NavLink to={`/blog/${blog.slug}`}>
                                                <Card className="uaf-fp_card"
                                                    cover={<img className="blog-featured-image" style={{ height: "180px" }} alt={blog.title} src={blog.featured_image} />}
                                                >
                                                    <Meta description={<div>
                                                        <p className="uaf-fp_card_heading uaf_bp_card-desc-heading">{blog.title}</p>
                                                        <p className="uaf_bp_card-desc uaf-fp_card_date"><span className="uaf_bp_card-desc-date">{moment(blog.date_addedd).format('ll')}, by </span>{blog.author}</p>
                                                    </div>} />
                                                </Card>
                                            </NavLink>
                                        </Col>

                                    })}
                                </Slider>
                            </Row>
                        </Col>
                        <Col xs={{ span: 24, order: 3 }} md={{ span: 24, order: 3 }} lg={{ span: 24, order: 3 }} className="sectionContent viewMoreBtn">
                            <NavLink to="/blog" target="_blank">{t('FacP_home_view')} <RightOutlined /></NavLink>
                        </Col>
                    </Row>
                </div>
            </div>
        </React.Fragment>
    );
}


BusinessOnline.propTypes = {
    getRecentBlog: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    recentBlog: state.BlogManagement.recentBlog,
});

export default connect(mapStateToProps, {
    getRecentBlog
})(BusinessOnline);
