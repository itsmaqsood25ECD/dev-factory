import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import ReCAPTCHA from "react-google-recaptcha";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { Redirect } from "react-router-dom";
import { CountryDropdown } from "react-country-region-selector";
import { Row, Col, Select, Button, Form, Input, Icon, message } from "antd";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { StudioContact } from '../actions/contact.actions'
import PropTypes from 'prop-types'
import '../assets/css/base/contactUs.css'
import { useTranslation } from 'react-i18next';


const recaptchaRef = React.createRef();
const FooterComp = ({
  StudioContact,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  direction
}) => {
  const { TextArea } = Input;
  const { Option } = Select;
  const [formData, setFormData] = useState({
    isSubmitted: false,
    HumanVarification: false,
  });

  const { t } = useTranslation();
  const {
    HumanVarification,
    isSubmitted
  } = formData;


  // Forms

  const selectCountry = val => {
    setFormData({
      ...formData,
      country: val
    });
  };

  const selectgettoknow = val => {
    setFormData({
      ...formData,
      hearedAt: val
    });
  };

  const onChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const onPhoneChange = e => {
    setFormData({
      ...formData,
      mobile: e
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        setFormData({
          ...formData
        });
        // console.log(formData, "body")
        StudioContact(formData);
        setFormData({
          isSubmitted: true
        });
        message.success('Request Successfull')
      }
    });
  };


  if (isSubmitted) {
    return <Redirect to="/success"></Redirect>;
  }

  const generalFooter = (
    <Fragment>
      <section id="uaf_cp_id_ContactUs" className="contactMain">
        <div className="UAF-container-fluid">
          <Row type="flex" align="middle" justify="center">
            <Col xs={{ span: 24, order: 1 }} md={{ span: 20, order: 1 }} lg={{ span: 18, order: 1 }} className="contactUsHeading">
              <h1>{t('FacP_contact_head')}</h1>
              <hr />
            </Col>
            <Col xs={{ span: 24, order: 1 }} md={{ span: 24, order: 2 }} lg={{ span: 18, order: 2 }} xl={{ span: 15, order: 2 }} className="contactUscontent">
              <Form onSubmit={e => onSubmit(e)} className="getin-touch-form contactUsFormMain">
                <Row gutter={30} className="contactform_InnerMain" gutter={30}>
                  <Col xs={{ span: 24, order: 1 }} md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }}>
                    <Form.Item>
                      {getFieldDecorator("name", {
                        rules: [
                          {
                            required: true,
                            message: `${t("FacP_contact_name_msg")}`
                          }
                        ]
                      })(
                        <Input
                          className="formInput"
                          placeholder={t("FacP_contact_name")}
                          name="firstname"
                          onChange={e => onChange(e)}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 24, order: 2 }} md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }}>
                    <Form.Item>
                      {getFieldDecorator("email", {
                        rules: [
                          {
                            required: true,
                            message: `${t("FacP_contact_email_msg")}`
                          }
                        ]
                      })(
                        <Input
                          className="formInput"
                          placeholder={t("FacP_contact_email")}
                          name="email"
                          type="email"
                          onChange={e => onChange(e)}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 24, order: 3 }} md={{ span: 12, order: 3 }} lg={{ span: 12, order: 3 }}>
                    <Form.Item>
                      {getFieldDecorator("number", {
                        rules: [
                          {
                            required: true,
                            message: `${t("FacP_contact_phone_msg")}`
                          }
                        ]
                      })(
                        <Input
                          className="formInput"
                          placeholder={t("FacP_contact_phone")}
                          name="number"
                          onChange={e => onChange(e)}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 24, order: 4 }} md={{ span: 12, order: 4 }} lg={{ span: 12, order: 4 }}>
                    <Form.Item>
                      {getFieldDecorator("country", {
                        rules: [
                          {
                            required: true,
                            message: `${t("FacP_contact_country_msg")}`
                          }
                        ]
                      })(
                        <CountryDropdown
                          className="uaf_cp_formCountryInput formInput"
                          blacklist={["AF","AO","DJ","GQ","ER","GA","IR","KG","LY","MD","NP","ST","SL","SD","SY","SR","TM","VE","ZW","IL"]}
                          name="country"
                          valueType="full"
                          defaultOptionLabel={t("FacP_contact_country")}
                          style={{ height: "32px" }}
                          onChange={val => selectCountry(val)}
                        />
                      )}
                    </Form.Item>{" "}
                  </Col>
                  <Col xs={{ span: 24, order: 5 }} md={{ span: 24, order: 5 }} lg={{ span: 24, order: 5 }}>
                    <Form.Item>
                      {getFieldDecorator("businessname", {
                        rules: [
                          {
                            required: true,
                            message: `${t("FacP_contact_bname_msg")}`
                          }
                        ]
                      })(
                        <Input
                          className="formInput"
                          placeholder={t("FacP_contact_bname")}
                          name="businessname"
                          onChange={e => onChange(e)}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 24, order: 6 }} md={{ span: 24, order: 6 }} lg={{ span: 24, order: 6 }}>
                    <Form.Item>
                      {getFieldDecorator("description", {
                        rules: [
                          {
                            required: true,
                            message: `${t("FacP_contact_bdesc_msg")}`
                          }
                        ]
                      })(
                        <TextArea
                          className="formInput"
                          name="description"
                          onChange={e => onChange(e)}
                          placeholder={t("FacP_contact_bdesc")}
                          autosize={{ minRows: 4, maxRows: 6 }}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col xs={{ span: 24, order: 7 }} md={{ span: 24, order: 7 }} lg={{ span: 24, order: 7 }}>
                    <Form.Item>
                      <Button
                        className="uaf_cp-speakwith-btn"
                        type="primary"
                        htmlType="submit"
                        style={{
                          height: "40px",
                          fontSize: "16px",
                          width: "100%",
                          textTransform: "uppercase",
                          fontWeight: "500"
                        }}
                      >
                        {t("FacP_contact_submit")}
                        </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        </div>
      </section>
    </Fragment>
  );

  return (
    <Fragment>
      {generalFooter}
    </Fragment>
  );
};

const FooterCompForm = Form.create({ name: "Get_in_Touch" })(FooterComp);

FooterCompForm.prototypes = {
  direction: withDirectionPropTypes.direction,
  StudioContact: PropTypes.func.isRequired,
};


export default connect(null, { StudioContact })(
  withDirection(FooterCompForm)
);
