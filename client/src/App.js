/* eslint react/prop-types: 0 */
import React, { Fragment, useState, useEffect } from "react";
import { Route, Switch } from "react-router-dom"
import { connect } from "react-redux";
import { loadUser } from './actions/auth.action';
import { store } from './store/store'
import DirectionProvider from 'react-with-direction/dist/DirectionProvider';
import HeaderComp from './components/Layouts/Header'
import FooterComp from './components/Layouts/Footer'
import HomeMain from './pages/index'

// Auth

import Login from './pages/Auth/Login/Login'
import Signup from './pages/Auth/Register/Register'
import ForgotPassword from "./pages/Auth/ForgetPassword";

// My Profile
import View from "./pages/Profile/View";
import Edit from "./pages/Profile/Edit";
import Orders from "./pages/Profile/Orders";


// Components
import ChooseWebsiteTheme from "./components/SingleVendor/ChooseWebsiteTheme";
import CheckoutSucess from './components/checkout/success'
import ThemeSelection from "./components/SingleVendor/ThemeSelection";
import CheckoutFailed from './components/checkout/Failed'
import Success from "./components/success"
// Pages
import MultiVendor from './pages/eCommerce-Multi-Vendor-Platform'
import SingleVendor from './pages/eCommerce-Single-Vendor-Platform'
import Restaurant from './pages/Restaurant'
import Checkout from "./pages/Checkout"
import Contact from './pages/Contact'
import Clientele from './pages/Clientele'
// Extra Pages
import AboutUs from './extrapages/Aboutus'
import CacelationRefundPolicy from './extrapages/CacelationRefundPolicy'
import TermsServices from './extrapages/TermsService'
import PrivacyPolicy from './extrapages/PrivacyPolicy'
import Blog from './extrapages/Blog/Blogpage'
import SingleBlog from './extrapages/Blog/Blog'
import WebsiteThemes from './components/SingleVendor/WebsiteThemes'
import SingleBlogRedirect from './extrapages/Blog/reDirectBlog'

import setAuthToken from './utils/setAuthToken'



let layoutDirection;
if (localStorage.getItem('lng') === 'AR') {
  layoutDirection = 'rtl';
  // i18next.changeLanguage('AR')
} else if (localStorage.getItem('lng') === 'EN') {
  layoutDirection = 'ltr';
  // i18next.changeLanguage('EN')
} else {
  layoutDirection = 'ltr';
  // i18next.changeLanguage('EN')
}

setAuthToken()
if (localStorage.token) {
  setAuthToken(localStorage.token);
}

function App() {


  useEffect(() => {
    store.dispatch(loadUser())

  }, [])


  return (
    <Fragment>
      <DirectionProvider direction={layoutDirection}>
        < HeaderComp />
      </DirectionProvider>
      <Switch>
        <section className="container">
          <DirectionProvider direction={layoutDirection}>
            <Route path="/" component={HomeMain} exact />
          </DirectionProvider>
          {/* Auth Start */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/login" component={Login} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/SignUp" component={Signup} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/forgot-password" component={ForgotPassword} exact />
          </DirectionProvider>

          {/* Auth End */}

          {/* My Profile */}

          <DirectionProvider direction={layoutDirection}>
            <Route path="/my-profile" component={View} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/edit-profile" component={Edit} exact />
          </DirectionProvider>

          <DirectionProvider direction={layoutDirection}>
            <Route path="/my-orders" component={Orders} exact />
          </DirectionProvider>

          {/* End My Profile */}

          {/* Multi Vendor Start*/}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Multi-Vendor-Platform" component={MultiVendor} exact />
          </DirectionProvider>
          {/* Multi Vendor End */}
          {/* Restaurant Start*/}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/restaurant" component={Restaurant} exact />
          </DirectionProvider>
          {/* Restaurant End */}
          
          {/* Single Vendor */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform" component={SingleVendor} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/Website-Themes" component={WebsiteThemes} exact />
          </DirectionProvider>
          
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan" component={ChooseWebsiteTheme} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan/:themeID" component={ThemeSelection} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan/:themeID/Checkout" component={Checkout} exact />
          </DirectionProvider>
          {/* Single Vendor End */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/success" component={Success} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan/:themeID/Checkout/success/:orderID" component={CheckoutSucess} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/eCommerce-Single-Vendor-Platform/:Package/:Plan/:themeID/Checkout/failed" component={CheckoutFailed} exact />
          </DirectionProvider>

          {/* Extra Pages Start */}
          <DirectionProvider direction={layoutDirection}>
            <Route path="/aboutus" component={AboutUs} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/cancellation-and-refund-policy" component={CacelationRefundPolicy} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/terms-service" component={TermsServices} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/privacy-policy" component={PrivacyPolicy} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/clientele" component={Clientele} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/blog" component={Blog} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/blog/:id" component={SingleBlog} exact />
          </DirectionProvider>
          <DirectionProvider direction={layoutDirection}>
            <Route path="/contactUs" component={Contact} exact />
          </DirectionProvider>
          {/* Extra Pages End */}

        </section>
      </Switch>
      <DirectionProvider direction={layoutDirection}>
        <FooterComp />
      </DirectionProvider>
    </Fragment >
  )
}

export default connect(null, null)(App);
