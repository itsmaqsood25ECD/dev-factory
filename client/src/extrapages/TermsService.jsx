import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import Hero from "../components/hero";
import 'react-phone-number-input/style.css';
import { Helmet } from "react-helmet";
import { Button, Typography, Form, Icon, Input, Row, Col, Select, Collapse, Affix, Card } from "antd";
import "antd/dist/antd.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';

const TermsServices = ({ direction }) => {

    const { t } = useTranslation();

    return (
        <Fragment>
            <Helmet>
                <title>Terms and Conditions - upappfactory</title>
                <meta
                    name="Terms and Conditions - upappfactory"
                    content="Terms and Conditions"
                />
            </Helmet>
            <Fragment>
                <Hero Title={t("FacP_tasp_Terms_Services")}></Hero>
                <div className="UAF-container-fluid" style={{ padding: "5rem 1rem", textAlign: "justify" }}>
                    <div className="Privacy-policy-container">
                        <p>{t("FacP_tasp_p1")}</p>
                        <p>{t("FacP_tasp_p2")}</p>
                        <p>{t("FacP_tasp_p3")}</p>
                        <ol>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li1_head")}</li>
                            <p>{t("FacP_tasp_li1_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li2_head")}</li>
                            <p>{t("FacP_tasp_li2_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li3_head")}</li>
                            <p>{t("FacP_tasp_li3_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li4_head")}</li>
                            <p>{t("FacP_tasp_li4_text1")} <strong>{t("FacP_tasp_li4_text2")}</strong> {t("FacP_tasp_li4_text3")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li5_head")}</li>
                            <p>{t("FacP_tasp_li5_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li6_head")}</li>
                            <p>{t("FacP_tasp_li6_text1")}</p>
                            <p>{t("FacP_tasp_li6_text2")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li7_head")}</li>
                            <p>{t("FacP_tasp_li7_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li8_head")}</li>
                            <p>{t("FacP_tasp_li8_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li9_head")}</li>
                            <p>{t("FacP_tasp_li9_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li10_head")}</li>
                            <p>{t("FacP_tasp_li10_text1")}</p>
                            <p>{t("FacP_tasp_li10_text2")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li11_head")}</li>
                            <p>{t("FacP_tasp_li11_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li12_head")}</li>
                            <p>{t("FacP_tasp_li12_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li13_head")}</li>
                            <p>{t("FacP_tasp_li13_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li14_head")}</li>
                            <p>{t("FacP_tasp_li14_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li15_head")}</li>
                            <p>{t("FacP_tasp_li15_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li16_head")}</li>
                            <p>{t("FacP_tasp_li16_text1")}</p>
                            <p>{t("FacP_tasp_li16_text2")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li17_head")}</li>
                            <p>{t("FacP_tasp_li17_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li18_head")}</li>
                            <p>{t("FacP_tasp_li18_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li19_head")}</li>
                            <p>{t("FacP_tasp_li19_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li20_head")}</li>
                            <p>{t("FacP_tasp_li20_text")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_tasp_li21_head")}</li>
                            <p>{t("FacP_tasp_li21_text")}</p>
                        </ol>
                    </div>
                </div>
            </Fragment>

        </Fragment>
    );
};



TermsServices.propTypes = {
    direction: withDirectionPropTypes.direction,
};

export default connect()(withDirection(TermsServices));
