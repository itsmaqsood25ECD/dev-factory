import React, { Component, Fragment } from "react";
import Hero from "../components/hero";
import { connect } from "react-redux";
import { Card, Typography, Table } from "antd";
import "../assets/css/base/homeNew.css";
import { Helmet } from "react-helmet";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';

const { Title } = Typography;
const PrivacyPolicy = ({ direction }) => {

    const { t } = useTranslation();

    return (
        <Fragment>
            <Helmet>
                <title>Privacy Policy - UPappfactory</title>
                <meta name="Privacy Policy" content="Privacy Policy" />
            </Helmet>
            <Fragment>
                <Hero Title="Privacy Policy"></Hero>
                <div className="UAF-container-fluid" style={{ padding: "5rem 1rem", textAlign: "justify" }}>
                    <div style={{ textAlign: "center" }}><Title level={4}>PERSONAL DATA COLLECTED FOR THE FOLLOWING PURPOSES AND USING THE FOLLOWING SERVICES</Title></div>
                    <div className="Privacy-policy-container">
                        <ul className="list-cirle heading">
                            <li>ANALYTICS</li>
                        </ul>
                        <ul>
                            <p>Personal Data: Cookies; Usage Data</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>CONTACTING THE USER</li>
                        </ul>
                        <ul>
                            <li className="pp-sub-heading-list" >MAILING LIST OR NEWSLETTER</li>
                            <p>Personal Data: email address; first name; last name</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>HOSTING AND BACKEND INFRASTRUCTURE</li>
                        </ul>
                        <ul>
                            <li className="pp-sub-heading-list" >AMAZON WEB SERVICES (AWS)</li>
                            <p>Personal Data: various types of Data as specified in the privacy policy of the service</p>
                            <li className="pp-sub-heading-list" >Heroku</li>
                            <p>Heroku is a cloud platform as a service supporting several programming languages. One of the first cloud platforms, Heroku has been in development since June 2007.</p>
                            <li className="pp-sub-heading-list" >MongoDB</li>
                            <p>MongoDB is a cross-platform document-oriented database program. Classified as a NoSQL database program, MongoDB uses JSON-like documents with schema. MongoDB is developed by MongoDB Inc. and licensed under the Server Side Public License</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>MANAGING SUPPORT AND CONTACT REQUESTS</li>
                        </ul>
                        <ul>
                            <li className="pp-sub-heading-list">Tawk.to</li>
                            <p>Personal Data: various types of Data as specified in the privacy policy of the service</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>CONTACT INFORMATION</li>
                        </ul>
                        <ul>
                            <li className="pp-sub-heading-list" >OWNER AND DATA CONTROLLER</li>
                            <div className="pp-f12">UP Application Development SPC (Reg # 1354715), Singapore</div>
                            <div className="pp-f12">Owner contact email: contactus@upappfactory.com</div>
                        </ul>
                        <div style={{ textAlign: "center", marginTop: "30px", textDecoration: "underline" }}><Title level={2}>FULL POLICY</Title></div>
                        <ul className="list-cirle heading">
                            <li>OWNER AND DATA CONTROLLER</li>
                        </ul>
                        <ul>
                            <div className="pp-f12">UP Application Development SPC (Reg # 1354715), Singapore</div>
                            <div className="pp-f12">Owner contact email: contactus@upappfactory.com</div>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>TYPES OF DATA COLLECTED</li>
                        </ul>
                        <ul>
                            <p>Among the types of Personal Data that this Application collects, by itself or through third parties, there are: Cookies; Usage Data; email address; first name; last name.
                            Complete details on each type of Personal Data collected are provided in the dedicated sections of this privacy policy or by specific explanation texts displayed prior to the Data collection.
                            Personal Data may be freely provided by the User, or, in case of Usage Data, collected automatically when using this Application.
                            Unless specified otherwise, all Data requested by this Application is mandatory and failure to provide this Data may make it impossible for this Application to provide its services. In cases where this Application specifically states that some Data is not mandatory, Users are free not to communicate this Data without consequences to the availability or the functioning of the Service.
                            Users who are uncertain about which Personal Data is mandatory are welcome to contact the Owner.
                            Any use of Cookies – or of other tracking tools – by this Application or by the owners of third-party services used by this Application serves the purpose of providing the Service required by the User, in addition to any other purposes described in the present document and in the Cookie Policy, if available.
                Users are responsible for any third-party Personal Data obtained, published or shared through this Application and confirm that they have the third party's consent to provide the Data to the Owner.</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>MODE AND PLACE OF PROCESSING THE DATA</li>
                        </ul>
                        <ul>
                            <li className="pp-sub-heading-list">METHODS OF PROCESSING</li>
                            <p>The Owner takes appropriate security measures to prevent unauthorized access, disclosure, modification, or unauthorized destruction of the Data.
                The Data processing is carried out using computers and/or IT enabled tools, following organizational procedures and modes strictly related to the purposes indicated. In addition to the Owner, in some cases, the Data may be accessible to certain types of persons in charge, involved with the operation of this Application (administration, sales, marketing, legal, system administration) or external parties (such as third-party technical service providers, mail carriers, hosting providers, IT companies, communications agencies) appointed, if necessary, as Data Processors by the Owner. The updated list of these parties may be requested from the Owner at any time.</p>
                            <li className="pp-sub-heading-list">LEGAL BASIS OF PROCESSING</li>
                            <p>The Owner may process Personal Data relating to Users if one of the following applies:
                <ul>
                                    <li><p>Users have given their consent for one or more specific purposes. Note: Under some legislations the Owner may be allowed to process Personal Data until the User objects to such processing (“opt-out”), without having to rely on consent or any other of the following legal bases. This, however, does not apply, whenever the processing of Personal Data is subject to European data protection law;</p></li>
                                    <li><p>provision of Data is necessary for the performance of an agreement with the User and/or for any pre-contractual obligations thereof;</p></li>
                                    <li><p>processing is necessary for compliance with a legal obligation to which the Owner is subject</p></li>
                                    <li><p>processing is related to a task that is carried out in the public interest or in the exercise of official authority vested in the Owner;</p></li>
                                    <li><p>processing is necessary for the purposes of the legitimate interests pursued by the Owner or by a third party.</p></li>
                                </ul>
                                <p>In any case, the Owner will gladly help to clarify the specific legal basis that applies to the processing, and whether the provision of Personal Data is a statutory or contractual requirement, or a requirement necessary to enter into a contract.</p>
                            </p>
                            <li className="pp-sub-heading-list">PLACE</li>
                            <p>The Data is processed at the Owner's operating offices and in any other places where the parties involved in the processing are located.</p>
                            <p>Depending on the User's location, data transfers may involve transferring the User's Data to a country other than their own. To find out more about the place of processing of such transferred Data, Users can check the section containing details about the processing of Personal Data.</p>
                            <p>Users are also entitled to learn about the legal basis of Data transfers to a country outside the European Union or to any international organization governed by public international law or set up by two or more countries, such as the UN, and about the security measures taken by the Owner to safeguard their Data.</p>
                            <p>If any such transfer takes place, Users can find out more by checking the relevant sections of this document or inquire with the Owner using the information provided in the contact section.</p>
                            <li className="pp-sub-heading-list">RETENTION TIME</li>
                            <p>Personal Data shall be processed and stored for as long as required by the purpose they have been collected for.</p>
                            <p>Therefore:</p>
                            <p>
                                <ul>
                                    <li>Personal Data collected for purposes related to the performance of a contract between the Owner and the User shall be retained until such contract has been fully performed.</li>
                                    <li>Personal Data collected for the purposes of the Owner’s legitimate interests shall be retained if needed to fulfill such purposes. Users may find specific information regarding the legitimate interests pursued by the Owner within the relevant sections of this document or by contacting the Owner.</li>
                                </ul>
                                <p>The Owner may be allowed to retain Personal Data for a longer period whenever the User has given consent to such processing, if such consent is not withdrawn. Furthermore, the Owner may be obliged to retain Personal Data for a longer period whenever required to do so for the performance of a legal obligation or upon order of an authority.</p>
                                <br />
                                <p>Once the retention period expires, Personal Data shall be deleted. Therefore, the right to access, the right to erasure, the right to rectification and the right to data portability cannot be enforced after expiration of the retention period.</p>
                            </p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>THE PURPOSES OF PROCESSING</li>
                        </ul>
                        <ul>
                            <p>The Data concerning the User is collected to allow the Owner to provide its Services, as well as for the following purposes: Analytics, Hosting and backend infrastructure, managing support and contact requests and Contacting the User.</p>
                            <p>Users can find further detailed information about such purposes of processing and about the specific Personal Data used for each purpose in the respective sections of this document.</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>DETAILED INFORMATION ON THE PROCESSING OF PERSONAL DATA</li>
                        </ul>
                        <ul>
                            <p>Personal Data is collected for the following purposes and using the following services:</p>
                            <li className="pp-sub-heading-list">ANALYTICS</li>
                            <p>The services contained in this section enable the Owner to monitor and analyze web traffic and can be used to keep track of User behavior.</p>
                            <p>Personal Data collected: Cookies; Usage Data.</p>
                            <p>Place of processing: United States – Privacy Policy – Opt Out. Privacy Shield participant.</p>
                            <li className="pp-sub-heading-list">GOOGLE ANALYTICS WITH ANONYMIZED IP (GOOGLE INC.)</li>
                            <p>Google Analytics is a web analysis service provided by Google Inc. (“Google”). Google utilizes the Data collected to track and examine the use of this Application, to prepare reports on its activities and share them with other Google services.</p>
                            <p>Google may use the Data collected to contextualize and personalize the ads of its own advertising network. This integration of Google Analytics anonymizes your IP address. It works by shortening Users' IP addresses within member states of the European Union or in other contracting states to the Agreement on the European Economic Area. Only in exceptional cases will the complete IP address be sent to a Google server and shortened within the US.</p>
                            <p>Personal Data collected: Cookies; Usage Data.</p>
                            <p>Place of processing: United States – Privacy Policy – Opt Out. Privacy Shield participant.</p>
                            <li className="pp-sub-heading-list">CONTACTING THE USER</li>
                            <ul>
                                <li className="pp-sub-heading-list">  MAILING LIST OR NEWSLETTER (THIS APPLICATION)</li>
                                <p>By registering on the mailing list or for the newsletter, the User’s email address will be added to the contact list of those who may receive email messages containing information of commercial or promotional nature concerning this Application. Your email address might also be added to this list as a result of signing up to this Application or after making a purchase.</p>
                                <p>Personal Data collected: email address; first name; last name.</p>
                            </ul>
                            <li className="pp-sub-heading-list">HOSTING AND BACKEND INFRASTRUCTURE</li>
                            <p>This type of service has the purpose of hosting Data and files that enable this Application to run and be distributed as well as to provide a ready-made infrastructure to run specific features or parts of this Application. Some of these services work through geographically distributed servers, making it difficult to determine the actual location where the Personal Data are stored.</p>
                            <p className="pp-sub-heading-list">AMAZON WEB SERVICES (AWS) (AMAZON WEB SERVICES, INC.)</p>
                            <p>Amazon Web Services (AWS) is a hosting and backend service provided by Amazon Web Services, Inc.
                            Personal Data collected: various types of Data as specified in the privacy policy of the service.
                Place of processing: Ireland – Privacy Policy. Privacy Shield participant.</p>
                            <p className="pp-sub-heading-list">Heroku</p>
                            <p>Heroku is a cloud platform as a service supporting several programming languages. One of the first cloud platforms, Heroku has been in development since June 2007.</p>
                            <p className="pp-sub-heading-list">MongoDB</p>
                            <p>MongoDB is a cross-platform document-oriented database program. Classified as a NoSQL database program, MongoDB uses JSON-like documents with schema. MongoDB is developed by MongoDB Inc. and licensed under the Server Side Public License</p>
                            <li className="pp-sub-heading-list">MANAGING SUPPORT AND CONTACT REQUESTS</li>
                            <p>This type of service allows this Application to manage support and contact requests received via email or by other means, such as the contact form.</p>
                            <p>The Personal Data processed depend on the information provided by the User in the messages and the means used for communication (e.g. email address).</p>
                            <li className="pp-sub-heading-list">Tawk.to</li>
                            <p>Tawk.to is a support and contact request management service provided by Tawk.
                            Personal Data collected: various types of Data as specified in the privacy policy of the service.
                Place of processing: United States – Privacy Policy. Privacy Shield participant.</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>THE RIGHTS OF USERS</li>
                        </ul>
                        <ul>
                            <p>Users may exercise certain rights regarding their Data processed by the Owner.</p>
                            <p>Users have the right to do the following:</p>
                            <li className="pp-sub-heading-list">Withdraw their consent at any time.</li>
                            <p>Users have the right to withdraw consent where they have previously given their consent to the processing of their Personal Data.</p>
                            <li className="pp-sub-heading-list">Object to processing of their Data.</li>
                            <p>Users have the right to object to the processing of their Data if the processing is carried out on a legal basis other than consent. Further details are provided in the dedicated section below.</p>
                            <li className="pp-sub-heading-list">Access their Data.</li>
                            <p>Users have the right to learn if Data is being processed by the Owner, obtain disclosure regarding certain aspects of the processing and obtain a copy of the Data undergoing processing.</p>
                            <li className="pp-sub-heading-list">Verify and seek rectification.</li>
                            <p>Users have the right to verify the accuracy of their Data and ask for it to be updated or corrected.</p>
                            <li className="pp-sub-heading-list">Restrict the processing of their Data.</li>
                            <p>Users have the right, under certain circumstances, to restrict the processing of their Data. In this case, the Owner will not process their Data for any purpose other than storing it.</p>
                            <li className="pp-sub-heading-list">Have their Personal Data deleted or otherwise removed?</li>
                            <p>Users have the right, under certain circumstances, to obtain the erasure of their Data from the Owner.</p>
                            <li className="pp-sub-heading-list">Receive their Data and have it transferred to another controller.</li>
                            <p>Users have the right to receive their Data in a structured, commonly used and machine-readable format and, if technically feasible, to have it transmitted to another controller without any hindrance. This provision is applicable provided that the Data is processed by automated means and that the processing is based on the User's consent, on a contract which the User is part of or on pre-contractual obligations thereof.</p>
                            <li className="pp-sub-heading-list">Lodge a complaint.</li>
                            <p>Users have the right to bring a claim before their competent data protection authority.</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>DETAILS ABOUT THE RIGHT TO OBJECT TO PROCESSING</li>
                        </ul>
                        <ul>
                            <p>Where Personal Data is processed for a public interest, in the exercise of an official authority vested in the Owner or for the purposes of the legitimate interests pursued by the Owner, Users may object to such processing by providing a ground related to their particular situation to justify the objection.
                Users must know that, however, should their Personal Data be processed for direct marketing purposes, they can object to that processing at any time without providing any justification. To learn, whether the Owner is processing Personal Data for direct marketing purposes, Users may refer to the relevant sections of this document.</p>
                            <li className="pp-sub-heading-list">HOW TO EXERCISE THESE RIGHTS</li>
                            <p>Any requests to exercise User rights can be directed to the Owner through the contact details provided in this document. These requests can be exercised free of charge and will be addressed by the Owner as early as possible and always within one month.</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>COOKIE POLICY</li>
                        </ul>
                        <ul>
                            <p>This Application uses Cookies. To learn more and for a detailed cookie notice, the User may consult the Cookie Policy.</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>ADDITIONAL INFORMATION ABOUT DATA COLLECTION AND PROCESSING</li>
                        </ul>
                        <ul>
                            <li className="pp-sub-heading-list">LEGAL ACTION</li>
                            <p>The User's Personal Data may be used for legal purposes by the Owner in Court or in the stages leading to possible legal action arising from improper use of this Application or the related Services.
                The User declares to be aware that the Owner may be required to reveal personal data upon request of public authorities.</p>
                            <li className="pp-sub-heading-list">ADDITIONAL INFORMATION ABOUT USER'S PERSONAL DATA</li>
                            <p>In addition to the information contained in this privacy policy, this Application may provide the User with additional and contextual information concerning Services or the collection and processing of Personal Data upon request.</p>
                            <li className="pp-sub-heading-list">SYSTEM LOGS AND MAINTENANCE</li>
                            <p>For operation and maintenance purposes, this Application and any third-party services may collect files that record interaction with this Application (System logs) use other Personal Data (such as the IP Address) for this purpose.</p>
                            <li className="pp-sub-heading-list">INFORMATION NOT CONTAINED IN THIS POLICY</li>
                            <p>More details concerning the collection or processing of Personal Data may be requested from the Owner at any time. Please see the contact information at the beginning of this document.</p>
                            <li className="pp-sub-heading-list">HOW “DO NOT TRACK” REQUESTS ARE HANDLED</li>
                            <p>This Application does not support “Do Not Track” requests.
                To determine whether any of the third-party services it uses honor the “Do Not Track” requests, please read their privacy policies.</p>
                            <li className="pp-sub-heading-list">CHANGES TO THIS PRIVACY POLICY</li>
                            <p>The Owner reserves the right to make changes to this privacy policy at any time by giving notice to its Users on this page and possibly within this Application and/or - as far as technically and legally feasible - sending a notice to Users via any contact information available to the Owner. It is strongly recommended to check this page often, referring to the date of the last modification listed at the bottom.</p>
                            <p>Should the changes affect processing activities performed based on the User’s consent, the Owner shall collect new consent from the User, where required.</p>
                        </ul>
                        <ul className="list-cirle heading">
                            <li>DEFINITIONS AND LEGAL REFERENCES</li>
                        </ul>
                        <ul>
                            <li className="pp-sub-heading-list">PERSONAL DATA (OR DATA)</li>
                            <p>Any information that directly, indirectly, or in connection with other information — including a personal identification number — allows for the identification or identifiability of a natural person.</p>
                            <li className="pp-sub-heading-list">USAGE DATA</li>
                            <p>Information collected automatically through this Application (or third-party services employed in this Application), which can include: the IP addresses or domain names of the computers utilized by the Users who use this Application, the URI addresses (Uniform Resource Identifier), the time of the request, the method utilized to submit the request to the server, the size of the file received in response, the numerical code indicating the status of the server's answer (successful outcome, error, etc.), the country of origin, the features of the browser and the operating system utilized by the User, the various time details per visit (e.g., the time spent on each page within the Application) and the details about the path followed within the Application with special reference to the sequence of pages visited, and other parameters about the device operating system and/or the User's IT environment.</p>
                            <li className="pp-sub-heading-list">USER</li>
                            <p>The individual using this Application who, unless otherwise specified, coincides with the Data Subject.</p>
                            <li className="pp-sub-heading-list">DATA SUBJECT</li>
                            <p>The natural person to whom the Personal Data refers.</p>
                            <li className="pp-sub-heading-list">DATA PROCESSOR (OR DATA SUPERVISOR)</li>
                            <p>The natural or legal person, public authority, agency or other body which processes Personal Data on behalf of the Controller, as described in this privacy policy.</p>
                            <li className="pp-sub-heading-list">DATA CONTROLLER (OR OWNER)</li>
                            <p>The natural or legal person, public authority, agency or other body which, alone or jointly with others, determines the purposes and means of the processing of Personal Data, including the security measures concerning the operation and use of this Application. The Data Controller, unless otherwise specified, is the Owner of this Application.</p>
                            <li className="pp-sub-heading-list">DATA CONTROLLER (OR OWNER)</li>
                            <p>The natural or legal person, public authority, agency or other body which, alone or jointly with others, determines the purposes and means of the processing of Personal Data, including the security measures concerning the operation and use of this Application. The Data Controller, unless otherwise specified, is the Owner of this Application.</p>
                            <li className="pp-sub-heading-list">THIS APPLICATION</li>
                            <p>The means by which the Personal Data of the User is collected and processed.</p>
                            <li className="pp-sub-heading-list">SERVICE</li>
                            <p>The service provided by this Application as described in the relative terms (if available) and on this site/application.</p>
                            <p style={{ fontSize: "10px" }}>Latest update: Nov 28, 2019</p>
                        </ul>
                    </div>
                </div>
            </Fragment>
        </Fragment>
    );
};



PrivacyPolicy.propTypes = {
    direction: withDirectionPropTypes.direction,
};

export default connect()(withDirection(PrivacyPolicy));
