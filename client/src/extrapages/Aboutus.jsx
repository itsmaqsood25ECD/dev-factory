import React, { Fragment } from 'react'
import Title from "antd/lib/typography/Title";
import { Row, Col } from "antd";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

// Images
import ourMisson from "../assets/img/aboutus/ourMission.webp";
import ourVision from "../assets/img/aboutus/ourVision.webp";
import customerFirst from "../assets/img/aboutus/customer-first.svg";
import innovationKey from "../assets/img/aboutus/innovation_is_key.svg";
import helpingToSucess from "../assets/img/aboutus/Helpingyoutosucceed.svg";

const Aboutus = ({ direction }) => {
    const { t } = useTranslation();
    return (
        <Fragment>
            <Helmet>
                <title>About Us - About UPappfactory</title>
                <meta
                    name="We are UpApp Factory and we are here to create an amazing channel between your brand and your customers, we call it a state-of-the-art native mobile app on iOS & Android along with amazing backend system with immense features to work on."
                    content="about on UPappfactory information About UPappfactory"
                />
            </Helmet>
            <div className="mainFooterBody" style={{ maxWidth: "1920px" }}>
                <div className="lg-ltr">
                    <div className="about-us-section" style={{ textAlign: "center", paddingTop: "50px" }}>
                        <div>
                            <Title className="light-text" level={1}>{t("FacP_ausp_AboutUS")}</Title>
                            <div className="lg-ltr" style={{ marginTop: "50px" }}>
                                <p>{t("FacP_ausp_section1_p1")}</p>
                                <p>{t("FacP_ausp_section1_p2")}</p>
                                <p>{t("FacP_ausp_section1_p3")}</p>
                                <p>{t("FacP_ausp_section1_p4")}</p>
                                <p>{t("FacP_ausp_section1_p5")} <a href="mailto:sales@upappfactory.com"> sales@upappfactory.com</a></p>
                            </div>
                        </div>
                    </div>
                    <Row type="flex" justify="space-around" align="middle" style={{ marginTop: "100px" }}>
                        <Col xs={{ span: 24, order: 1 }} lg={{ span: 12, order: 1 }}>
                            <img className="our-mission" src={ourMisson} alt="" />
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} lg={{ span: 12, order: 2 }}>
                            <div>
                                <Title className="light-text text-center" level={1}>
                                    {t("FacP_ausp_section2_head")}
                                </Title>
                                <p className="mission-msg text-center">
                                    {t("FacP_ausp_section2_text")}
                                </p>
                            </div>
                        </Col>
                    </Row>
                    <Row type="flex" justify="space-around" align="middle">
                        <Col xs={{ span: 24, order: 2 }} lg={{ span: 12, order: 1 }}>
                            <div>
                                <Title className="light-text text-center" level={1}>
                                    {t("FacP_ausp_section3_head")}
                                </Title>
                                <p className="vision-msg text-center">
                                    {t("FacP_ausp_section3_text")}
                                </p>
                            </div>
                        </Col>
                        <Col xs={{ span: 24, order: 1 }} lg={{ span: 12, order: 2 }}>
                            <img className="our-mission" src={ourVision} alt="" />
                        </Col>
                    </Row>
                    <div className="text-center" style={{ marginTop: "50px" }}>
                        <Title className="light-text" level={1}>
                            {t("FacP_ausp_section4_head")}
                        </Title>
                    </div>
                    <Row style={{ marginTop: "80px" }}>
                        <Col xs={24} lg={8}>
                            <div className="upapp-value-container">
                                <img src={customerFirst} alt="" />
                                <p>
                                    {t("FacP_ausp_section4_text1")}
                                </p>
                            </div>
                        </Col>
                        <Col xs={24} lg={8}>
                            <div className="upapp-value-container">
                                <img src={innovationKey} alt="" />
                                <p>
                                    {t("FacP_ausp_section4_text2")}
                                </p>
                            </div>
                        </Col>
                        <Col xs={24} lg={8}>
                            <div className="upapp-value-container">
                                <img src={helpingToSucess} alt="" />
                                <p>
                                    {t("FacP_ausp_section4_text3")}
                                </p>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </Fragment>
    );
}
const mapStateToProps = state => ({});
export default connect(mapStateToProps, {})(withDirection(Aboutus));