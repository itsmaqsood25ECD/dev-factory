import React, { Fragment } from 'react'
import Title from "antd/lib/typography/Title";
import { Table } from "antd";
import { Helmet } from "react-helmet";
import Hero from "../components/hero";
import { connect } from "react-redux";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import "../assets/css/base/custom.css";


// Images

const CancelationRefundPolicy = ({ direction }) => {

    const { t } = useTranslation();

    const columns = [
        {
            title: `${t("FacP_carpp_month")}`,
            dataIndex: "Month",
            key: "Month"
        },
        {
            title: `${t("FacP_carpp_cost")}`,
            dataIndex: "Cost",
            key: "Cost"
        },
        {
            title: `${t("FacP_carpp_website")}`,
            dataIndex: "Website",
            key: "Website"
        },
        {
            title: `${t("FacP_carpp_mobileApp")}`,
            dataIndex: "App",
            key: "App"
        },
        {
            title: `${t("FacP_carpp_mobileApp_website")}`,
            dataIndex: "WebApp",
            key: "WebApp"
        }
    ];

    const data = [
        {
            key: "1",
            Month: "1 - 3 Months",
            Cost: "NA",
            Website: "NA",
            App: "NA",
            WebApp: "NA"
        },
        {
            key: "2",
            Month: "4 - 6 Months",
            Cost: "80%",
            Website: "$ 240",
            App: "$ 400 ",
            WebApp: "$ 560"
        },
        {
            key: "3",
            Month: "7 - 9 Months",
            Cost: "60%",
            Website: "$ 180",
            App: "$ 300 ",
            WebApp: "$ 420"
        },
        {
            key: "4",
            Month: "10+ Months",
            Cost: "30%",
            Website: "$ 90",
            App: "$ 150 ",
            WebApp: "$ 210"
        }
    ];


    return (
        <Fragment>
            <Helmet>
                <title>About Us - About UPappfactory</title>
                <meta
                    name="We are UpApp Factory and we are here to create an amazing channel between your brand and your customers, we call it a state-of-the-art native mobile app on iOS & Android along with amazing backend system with immense features to work on."
                    content="about on UPappfactory information About UPappfactory"
                />
            </Helmet>
            <Fragment>
                <Hero Title={t("FacP_carpp_head")}></Hero>
                <div className="UAF-container-fluid" style={{ padding: "5rem 1rem", textAlign: "justify" }}>
                    <div className="Privacy-policy-container">
                        <p>{t("FacP_carpp_p1")}</p>
                        <ul>
                            <li className="crp-sub-heading-list">{t("FacP_carpp_head_l1")}</li>
                            <p>{t("FacP_carpp_text_l1")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_carpp_head_l2")}</li>
                            <p>{t("FacP_carpp_text_l2")}</p>
                            <ol>
                                <li>
                                    <p>{t("FacP_carpp_text_l2_p1")}</p>
                                </li>
                            </ol>
                            <li className="crp-sub-heading-list">{t("FacP_carpp_head_l3")}</li>
                            <p>{t("FacP_carpp_text_l3_p1")}</p>
                            <p>{t("FacP_carpp_text_l3_p2")} <strong>{t("FacP_carpp_text_l3_p3")}</strong>{t("FacP_carpp_text_l3_p4")}
                                <strong> {t("FacP_carpp_text_l3_p5")}</strong></p>
                            <p>{t("FacP_carpp_text_l3_p6")} <a href="mailto:contactus@upappfactory.com" style={{ color: "rgba(0, 0, 0, 0.65)" }}>contactus@upappfactory.com.</a></p>

                            <li className="crp-sub-heading-list">{t("FacP_carpp_head_l4")}</li>
                            <p>{t("FacP_carpp_text_l4")}</p>
                            <li className="crp-sub-heading-list">{t("FacP_carpp_head_l5")}</li>
                            <p>
                                <Table columns={columns} pagination={false} dataSource={data} className="uaf_crppage_entable" />
                            </p>
                        </ul>
                    </div>
                </div>
            </Fragment>
        </Fragment>
    );
};

CancelationRefundPolicy.propTypes = {
    direction: withDirectionPropTypes.direction,
};

export default connect()(withDirection(CancelationRefundPolicy));