import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom'
import { Provider } from "react-redux"
import { store, persistor } from './store/store'
import { PersistGate } from 'redux-persist/integration/react'
import { Spin } from 'antd'

import App from './App';
import createHistory from 'history/createBrowserHistory'

import './i18n';

const history = createHistory()

ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={(<Spin />)}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router history={history}>
            <App />
          </Router>
        </PersistGate>
      </Provider>
    </Suspense>
  </React.StrictMode>,
  document.getElementById('root')
);
