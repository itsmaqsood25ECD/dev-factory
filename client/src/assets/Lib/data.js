export default [{
        productName:"eCommerce-Single-Vendor-Platform",
        productID:'001',
        packages: [
            {
              type: "Basic",
              typeAR: "العادية",
              includes: ["eCommerce Website Included"],
              includesAR: ["يشمل موقع للتجارة الإلكترونية"],
              plan: [
               {
                  Name: "Standard",
                  NameAR: "أساسي",
                  planCosts:[
                    {
                      before_discount_price: 29.9,
                      after_discount_price: 19.9,
                      OneTimeSetupCost:500,
                      value: 'standard-monthly',
                      type: "Monthly",
                      typeAR:"شهري"
                      
                    },
                  {
                      before_discount_price: 29.9,
                      after_discount_price: 9.9,
                      OneTimeSetupCost:500,
                      value: 'standard-yearly',
                      type: "Yearly",
                      typeAR:"سنوي"
                    }
                  ]
                },
                // {
                //   Name: "Rental",
                //   NameAR: "الإيجار",
                //   planCosts:[{
                //     before_discount_price: 69.9,
                //     after_discount_price: 59.9,
                //     OneTimeSetupCost:0,
                //     value: 'rental-monthly',
                //     type: "Monthly",
                //     typeAR:"شهري"
                //   }]
                // },
              ],
            },
            {
              type: "Ultra",
              typeAR: "الفائقة",
              includes: ["Android | iPhone App included", "eCommerce Website Included"],
              includesAR: ["يشمل تطبيق أندرويد \\ iOS ", "يشمل موقع للتجارة الإلكترونية"],
              AdditionalLanguageCost:100,
              plan: [
                {
                  Name: "Standard",
                  NameAR: "أساسي",
                  planCosts:[{
                      before_discount_price: 49.9,
                      after_discount_price: 39.9,
                      OneTimeSetupCost:1200,
                      value: 'standard-monthly',
                      type: "Monthly",
                      typeAR:"شهري"
                    },
                    {
                      before_discount_price: 49.9,
                      after_discount_price: 29.9,
                      OneTimeSetupCost:1200,
                      value: 'standard-yearly',
                      type: "Yearly",
                      typeAR:"سنوي"
                    }]
                },
                // {
                //   Name: "Rental",
                //   NameAR: "الإيجار",
                //   planCosts:[{
                //       before_discount_price: 149.9,
                //       after_discount_price: 119.9,
                //       OneTimeSetupCost:0,
                //       value: 'rental-monthly',
                //       type: "Monthly",
                //       typeAR:"شهري"
                //     }]
                // },
              ],
            },
            {
              type: "Premium",
              typeAR: "الممتازة",
              AdditionalLanguageCost:100,
              includes: ["Android | iPhone App included"],
              includesAR: ["يشمل تطبيق أندرويد \\ iOS "],
              plan: [
                {
                  Name: "Standard",
                  NameAR:"أساسي",
                  planCosts: [{
                      before_discount_price: 39.9,
                      after_discount_price: 29.9,
                      OneTimeSetupCost:800,
                      value: 'standard-monthly',
                      type: "Monthly",
                      typeAR:"شهري"
                    },
                    {
                      before_discount_price: 39.9,
                      after_discount_price: 19.9,
                      OneTimeSetupCost:800,
                      value: 'standard-yearly',
                      type: "Yearly",
                      typeAR:"سنوي"
                    }],
                },
                // {
                //   Name: "Rental",
                //   NameAR:"الإيجار",
                //   planCosts:[{
                //       before_discount_price: 119.9,
                //       after_discount_price: 89.9,
                //       OneTimeSetupCost:0,
                //       value: 'rental-monthly',
                //       type: "Monthly",
                //       typeAR:"شهري"
                //     }]
                // },
              ],
            }
          ],
          PaymentGateway:[
            {
             name:"Stripe",
             nameAR:"Stripe",
             img:"https://upload.wikimedia.org/wikipedia/commons/2/2a/Stripe_logo%2C_revised_2014.png",
             SetupCost:"0"
            },
            {
             name:"Paypal",
             nameAR:"Paypal",
             img:"https://cdn.pixabay.com/photo/2015/05/26/09/37/paypal-784404_960_720.png",
             SetupCost:"0"
            }
          ],
        ShippingProvider:[
         {
          name:"Aramex",
          nameAR:"Aramex",
          img:"https://logovectorseek.com/wp-content/uploads/2019/08/aramex-logo-vector.png",
          SetupCost:"0"
         },
         {
          name:"FedEx",
          nameAR:"FedEx",
          img:"https://logos-download.com/wp-content/uploads/2016/06/FedEx_Express_logo.png",
          SetupCost:"0"
         }
          ],
        AdditionalLanguageCost:100,
        },
    ]