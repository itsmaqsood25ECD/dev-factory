import { USER_LOCATION, LOCATION_FAIL } from './constants';
import axios from 'axios';

export const getLocation = () => async (dispatch) => {
	try {
		const userlocation = await axios.get('/');
		// console.log(userlocation);
		dispatch({
			type: USER_LOCATION,
			payload: userlocation
		});
	} catch (error) {
		console.error(error);
	}
};
