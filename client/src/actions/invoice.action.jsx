import axios from "axios";
import { GET_INVOICE, INVOICE_ERROR } from "./constants";

//Get Orders
export const getInvoice = () => async dispatch => {
	try {
		const res = await axios.get("/get-orders-details");
		dispatch({
			type: GET_ORDERS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: ORDER_ERROR
		});
	}
};
