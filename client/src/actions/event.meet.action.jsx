import axios from "axios";
import { GET_EVENT, EVENT_SUCCESS, EVENT_FAIL } from "./constants";

// Conatct Form Submission
export const eventMeet = ({
  name,
  email,
  mobile,
  city,
  country,
  description,
  designation
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    name,
    email,
    mobile,
    city,
    country,
    description,
    designation
  });

  try {
    const res = await axios.post("/event-request", body, config);
    dispatch({
      type: EVENT_SUCCESS,
      payload: res.data
    });
    console.log(res.data);
  } catch (error) {
    console.error(error);
    dispatch({
      type: EVENT_FAIL
    });
  }
};

// Request for customised app
export const getEventMeet = () => async dispatch => {
  try {
    const res = await axios.get("/event-request");
    // console.log(res.data.result);
    dispatch({
      type: EVENT_SUCCESS,
      payload: res.data.result
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: EVENT_SUCCESS
    });
  }
};
