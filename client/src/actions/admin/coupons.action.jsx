import axios from "axios";
import {
	GET_COUPONS,
	COUPONS_ERROR,
	GET_COUPON_DETAILS,
	COUPON_FAILURE,
	COUPON_SUCCESS
} from "../constants";

//Get Coupons
export const getCoupons = () => async dispatch => {
	try {
		const res = await axios.get("/coupon-management/");
		dispatch({
			type: GET_COUPONS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: COUPONS_ERROR
		});
	}
};
//Get Coupons code
export const getCouponCode = coupon => async dispatch => {
	try {
		let res;
		if (process.env.NODE_ENV === "production") {
			res = await axios.get(`http://www.upappfactory.com/${coupon}`);
		} else res = await axios.get(`/coupon-management/${coupon}`);
		dispatch({
			type: GET_COUPON_DETAILS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: COUPON_FAILURE
		});
	}
};

//Delete Coupon Code
export const deleteCouponCode = coupon => async dispatch => {
	try {
		let res;
		if (process.env.NODE_ENV === "production") {
			res = await axios.delete(`http://www.upappfactory.com/${coupon}`);
		} else res = await axios.delete(`/coupon-management/${coupon}`);
	} catch (error) {
		console.error(error);
	}
};

// Coupon creation
export const createCoupon = ({
	coupon_code,
	coupon_description,
	coupon_pack_type,
	discount_type,
	coupon_amount,
	coupon_expiry_date,
	min_spend,
	max_spend,
	prod,
	exclude_prod,
	include_categ,
	exclude_categ,
	allowed_emails,
	usage_limit_per_coupon,
	usage_limit_x_items,
	usage_limit_per_user
}) => async dispatch => {
	const config = {
		headers: {
			"Content-Type": "application/json"
		}
	};
	const products = prod.map(arg => {
		return arg.key;
	});
	const exclude_products = exclude_prod.map(arg => {
		return arg.key;
	});
	const include_category = include_categ.map(arg => {
		return arg.key;
	});
	const exclude_category = exclude_categ.map(arg => {
		return arg.key;
	});
	const body = JSON.stringify({
		coupon_code,
		coupon_description,
		general: {
			discount_type,
			coupon_amount,
			coupon_expiry_date,
			coupon_pack_type
		},
		usage_restriction: {
			min_spend,
			max_spend,
			products,
			exclude_products,
			include_category,
			exclude_category,
			allowed_emails
		},
		usage_limits: {
			usage_limit_per_coupon,
			usage_limit_x_items,
			usage_limit_per_user
		}
	});
	try {
		const res = await axios.post("/coupon-management", body, config);
		dispatch({
			type: COUPON_SUCCESS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		// dispatch({
		// 	type: CONTACT_FAIL
		// });
	}
};

// Coupon updation
export const updateCoupon = ({
	coupon_code,
	coupon_description,
	coupon_pack_type,
	discount_type,
	coupon_amount,
	coupon_expiry_date,
	min_spend,
	max_spend,
	prod,
	exclude_prod,
	include_categ,
	exclude_categ,
	allowed_emails,
	usage_limit_per_coupon,
	usage_limit_x_items,
	usage_limit_per_user
}) => async dispatch => {
	const config = {
		headers: {
			"Content-Type": "application/json"
		}
	};
	const products = prod.map(arg => {
		return arg.key;
	});
	const exclude_products = exclude_prod.map(arg => {
		return arg.key;
	});
	const include_category = include_categ.map(arg => {
		return arg.key;
	});
	const exclude_category = exclude_categ.map(arg => {
		return arg.key;
	});
	const body = JSON.stringify({
		coupon_code,
		coupon_description,
		general: {
			discount_type,
			coupon_amount,
			coupon_expiry_date,
			coupon_pack_type
		},
		usage_restriction: {
			min_spend,
			max_spend,
			products,
			exclude_products,
			include_category,
			exclude_category,
			allowed_emails
		},
		usage_limits: {
			usage_limit_per_coupon,
			usage_limit_x_items,
			usage_limit_per_user
		}
	});
	try {
		const res = await axios.put(
			`/coupon-management/${coupon_code}`,
			body,
			config
		);
		dispatch({
			type: COUPON_SUCCESS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		// dispatch({
		// 	type: CONTACT_FAIL
		// });
	}
};
