import axios from "axios"
import { ALL_CUSTOMER_SUCCESS, GET_CUSTOMER_SUCCESS, ALL_CUSTOMER_FAIL, GET_CUSTOMER_FAIL } from "../constants"

//Get All Customers
export const GetAllCustomers = () => async dispatch =>  {
  try {
    const res = await axios.get('/get_all_users')
    dispatch({
      type: ALL_CUSTOMER_SUCCESS,
      payload: res.data
    })
  } catch (error){
    console.error(error)
    dispatch({
      type:ALL_CUSTOMER_FAIL
    })
  }
}

export const GetCustomers = (id) => async dispatch =>  {
  try {
    const res = await axios.get(`/get_customer_details/${id}`)
    dispatch({
      type: GET_CUSTOMER_SUCCESS,
      payload: res.data
    })
    
  } catch (error) {
    console.error(error)
    dispatch({
      type:GET_CUSTOMER_FAIL
    })
  }
}

export const DeleteCustomer = (id) => async dispatch =>  {
  try {
    let res;
     res = await axios.delete(`/delete-user/${id}`)
   
  } catch (error) {
    console.error(error)
  
  }
}