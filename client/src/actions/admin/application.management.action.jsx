import axios from 'axios';
import {
	ALL_APPLICATION_SUCCESS,
	APPLICATION_FAIL,
	APPLICATION_SUCCESS,
	APPLICATION_LOAD,
	DELETE_FAIL,
	DELETE_SUCCESS
} from '../constants';

//GET Application Data
export const GetApplication = () => async dispatch => {
	try {
		const res = await axios.get('/admin_application_management');
		console.log(res.data, "applcations")
		dispatch({
			type: ALL_APPLICATION_SUCCESS,
			payload: res.data
		});
	} catch (error) {
		dispatch({
			type: APPLICATION_FAIL
		});
	}
};

//GET Application Data by id
export const GetApplicationById = id => async dispatch => {
	try {
		const res = await axios.get(`/admin_application_management/${id}`);
		dispatch({
			type: APPLICATION_LOAD,
			payload: res.data
		});
		return res.data;
	} catch (error) {
		dispatch({
			type: APPLICATION_FAIL
		});
	}
};

//POST Application Data
export const applicationPost = ({
	category,
	featureImg,
	thumbImg,
	name,
	nameAR,
	type,
	industry,
	screenshots,
	desc,
	descAR,
	bdspm,
	adspm,
	bdspy,
	adspy,
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	let slug =
		name
			.toLowerCase()
			.split(' ')
			.join('-');
	const body = JSON.stringify({
		fields: {
			slug,
			category,
			type,
			industry,
			images: [
				{
					thumb: {
						url: thumbImg
					},
					featured: {
						url: featureImg
					},
					screenshots
				}
			],
			// thumb: {
			// 	url: thumbImg
			// },
			// featured: {
			// 	url: featureImg
			// },
			// screenshots,
			name,
			desc,
			ar: {
				name: nameAR,
				desc: descAR,
			},
			month: {
				type: 'Month',
				bdspm,
				adspm
			},
			year: {
				type: 'Year',
				bdspy,
				adspy
			},
		}
	});
	try {
		const res = await axios.post('/application-management', body, config);
		dispatch({
			type: APPLICATION_SUCCESS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: APPLICATION_FAIL
		});
	}
};

//POST Application Data
export const updateApplication = ({
	category,
	featureImg,
	thumbImg,
	name,
	nameAR,
	type,
	industry,
	screenshots,
	desc,
	descAR,
	bdspm,
	adspm,
	bdspy,
	adspy,
	screenshotsEdit,
	id
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	let slug =
		name
			.toLowerCase()
			.split(' ')
			.join('-');
	const body = JSON.stringify({
		fields: {
			slug,
			category,
			type,
			industry,
			images: [
				{
					thumb: {
						url: thumbImg
					},
					featured: {
						url: featureImg
					},
					screenshots: (screenshots && screenshots.length > 0) ? screenshots.concat(screenshotsEdit) : screenshotsEdit
				}
			],
			// thumb: {
			// 	url: thumbImg
			// },
			// featured: {
			// 	url: featureImg
			// },
			// screenshots: screenshots.concat(screenshotsEdit),
			name,
			desc,
			ar: {
				name: nameAR,
				desc: descAR,
			},
			month: {
				type: 'Month',
				bdspm,
				adspm
			},
			year: {
				type: 'Year',
				bdspy,
				adspy
			},
		}
	});
	console.log(body, "body")
	try {
		const res = await axios.patch(
			`/application-management/${id}`,
			body,
			config
		);
		dispatch({
			type: APPLICATION_SUCCESS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: APPLICATION_FAIL
		});
	}
};

export const DeleteApplication = (id) => async dispatch => {
	try {
		const res = await axios.delete(`/application-management/${id}`)
		dispatch({ type: DELETE_SUCCESS })
	} catch (error) {
		console.log(error)
		dispatch({ type: DELETE_FAIL })
	}
}
