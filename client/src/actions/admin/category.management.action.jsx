import axios from "axios";
import { CATEGORY_SUCCESS, CATEGORY_FAIL, LOAD_CATEGORY } from "../constants";

//POST category Data
export const createCategory = ({ name, type, nameAR }) => async dispatch => {
	const config = {
		headers: {
			"Content-Type": "application/json"
		}
	};
	const body = JSON.stringify({
		name,
		nameAR,
		type
	});
	console.log(body)
	try {
		const res = await axios.post("/category-management", body, config);
		dispatch({
			type: CATEGORY_SUCCESS,
			payload: res.data
		});
	} catch (error) {
		console.log(error);
		dispatch({
			type: CATEGORY_FAIL
		});
	}
};

//PUT category Data
export const updateCategory = ({ name, type, nameAR, id }) => async dispatch => {
	const config = {
		headers: {
			"Content-Type": "application/json"
		}
	};
	const body = JSON.stringify({
		name,
		type,
		nameAR
	});
	try {
		const res = await axios.put(`/category-management/${id}`, body, config);
		dispatch({
			type: CATEGORY_SUCCESS,
			payload: res.data
		});
	} catch (error) {
		console.log(error);
		dispatch({
			type: CATEGORY_FAIL
		});
	}
};
//get category Data
export const GetCategory = () => async dispatch => {
	try {
		const res = await axios.get("/category-management");
		dispatch({
			type: LOAD_CATEGORY,
			payload: res.data
		});
	} catch (error) {
		console.log(error);
		dispatch({
			type: CATEGORY_FAIL
		});
	}
};

//get category Data
export const GetCategoryById = id => async dispatch => {
	try {
		const res = await axios.get(`/category-management/${id}`);
		dispatch({
			type: CATEGORY_SUCCESS,
			payload: res.data
		});
		return res.data.result;
	} catch (error) {
		console.log(error);
		dispatch({
			type: CATEGORY_FAIL
		});
	}
};
