import { ROLE_SUCCESS, ROLE_FAIL } from './constants';

// Country Selector Submission
export const ChangeRole = ({
	id,
	role
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({
		id,
		role
	});
    console.log(body);
	try {
		const res = await axios.post(`/user-role/${id}`, body, config);
		dispatch({
			type: ROLE_SUCCESS,
			payload: body
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: ROLE_FAIL
		});
	}
};
