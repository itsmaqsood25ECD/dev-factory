const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const Schema = mongoose.Schema;


const UserSchema = new Schema({
	UID: {
		type: String
	},
	firstName: {
		type: String,
		trim: true,
		required: true
	},
	lastName: {
		type: String,
		trim: true,
		required: true
	},
	email: {
		type: String,
		required: true,
		lowercase: true,
		unique: true,
		validate(value) {
			if (!validator.isEmail(value)) {
				throw new Error('please type in a valid email')
			}
		}
	},
	password: {
		type: String,
		required: true,
		min: 7
	},
	age: {
		type: Number,
	},
	gender: {
		type: String,
	},
	country: {
		type: String,
		required: true
	},
	phone: {
		type: String,
		required: true
	},
	tokens: [{
		token: {
			type: String,
			required: true
		}
	}],
	role: {
		type: String,
		default: "user"
	},
	createdAt: {
		type: Date,
		default: Date.now()
	},
	isVerified: {
		type: Boolean,
		default: false
	},
	line1: {
		type: String
	},
	line2: {
		type: String
	},
	city: {
		type: String
	},
	state: {
		type: String
	},
	postal_code: {
		type: String
	},
	coupon_used: [{
		coupon_code: {
			type: String
		},
		coupon_used_number: {
			type: Number
		}
	}]
})


UserSchema.methods.toJSON = function () {
	const user = this
	const userObject = user.toObject()

	delete userObject.password
	delete userObject.tokens
	return userObject
}
// generating auth token and storing on to user
UserSchema.methods.generateAuthToken = async function () {
	const user = this
	const token = jwt.sign({ _id: user._id.toString() }, 'thisismyecommdesktoken')

	user.tokens = user.tokens.concat({ token })
	await user.save()
	return token
}
// find user by credentials for login
UserSchema.statics.findByCredentials = async (email, password) => {
	const user = await User.findOne({ email })
	if (!user) {
		return ({ error: true, success: false, message: "Invalid Credentials", result: {} })
	}

	const isMatch = await bcrypt.compare(password, user.password)
	if (!isMatch) {
		return ({ error: true, success: false, message: "Invalid Credentials", result: {} })
	}

	return ({ error: false, success: true, message: "User Found", result: user })
}
// find user by credentials for login
UserSchema.statics.findUserById = async (id) => {
	const user = await User.findById(id)
	return user
}

// hash password middleware
UserSchema.pre('save', async function (next) {
	const user = this
	if (user.isModified('password')) {
		user.password = await bcrypt.hash(user.password, 8)
	}
	next()
})

const User = mongoose.model("User", UserSchema)
module.exports = User