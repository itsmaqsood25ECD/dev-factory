const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const ApplicationSchema = new Schema({
	UID: {
		type: String
	},
	fields: {
		slug: {
			type: String
		},
		type: {
			type: String
		},
		industry: {
			type: String
		},
		category: {
			type: String
		},
		images: [{
			thumb: {
				url: {
					type: String
				}
			},
			featured: {
				url: {
					type: String
				}
			},
			screenshots: [{
				name: {
					type: String
				},
				uid: {
					type: String
				},
				url: {
					type: String
				},
			}]
		}],
		// thumb: {
		// 	url: {
		// 		type: String
		// 	}
		// },
		// featured: {
		// 	url: {
		// 		type: String
		// 	}
		// },
		// screenshots: [{
		// 	name: {
		// 		type: String
		// 	},
		// 	uid: {
		// 		type: String
		// 	},
		// 	status: {
		// 		type: String
		// 	},
		// 	url: {
		// 		type: String
		// 	},
		// 	originFileObj: {
		// 		name: {
		// 			type: String
		// 		},
		// 		uid: {
		// 			type: String
		// 		},
		// 		url: {
		// 			type: String
		// 		}
		// 	}
		// }],
		month: {
			type: {
				type: String
			},
			bdspm: {
				type: Number
			},
			adspm: {
				type: Number
			}
		},
		year: {
			type: {
				type: String
			},
			bdspy: {
				type: Number
			},
			adspy: {
				type: Number
			}
		},
		name: {
			type: String
		},
		desc: {
			type: String
		},
		ar: {
			name: {
				type: String
			},
			desc: {
				type: String
			}
		}
	},
	createdBy: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	createdAt: {
		type: String,
		default: Date.now()
	}
})

// find user by credentials for login
ApplicationSchema.statics.findByName = async (name) => {
	const application = await Application.findOne({ "fields.name": name })
	return application
}

const Application = mongoose.model("Application", ApplicationSchema)
module.exports = Application