const mongoose = require('mongoose')

const Schema = mongoose.Schema


const categorySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    nameAR: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    createdAt: {
        type: String,
        default: Date.now(),
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
})

const Category = mongoose.model("Category", categorySchema)
module.exports = Category