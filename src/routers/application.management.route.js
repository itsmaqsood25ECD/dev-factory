const express = require('express')
const Application = require('../models/application.management.model')
const redis = require('redis');

const router = new express.Router()
let client

client = redis.createClient()

client.on('connect', function () {
	console.log('Redis client connected');
});
client.on('error', function (err) {
	console.log('Something went wrong ' + err);
});

router.post('/application-management', async (req, res) => {
	try {
		const prevApp = await Application.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevApp.length === 0) {
			counter = 1
		} else {
			counter = prevApp[0].UID.slice(7)
			counter = +counter + 1
		}
		if (req.body.fields.type === "Ecommerce") {
			UAF = "UAF-EC-"
		} else if (
			req.body.fields.type === "Booking"
		) {
			UAF = "UAF-BK-"
		}
		if (counter < 10) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}00${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const application = new Application(req.body)
		application.UID = UID
		await application.save()
		console.log(`An Application has been added successfully`)
		res.status(201).send(application)
	} catch (e) {
		console.error(e)
		res.status(400).send(e)
	}
})

router.get('/application-management', async (req, res) => {
	try {
		// applications = await Application.find({}).sort({ UID: -1 })
		// res.status(200).send(applications)
		let applications = []
		return client.get("Applications", async (err, result) => {
			if (result) {
				const resultJSON = JSON.parse(result)
				for (var i in resultJSON)
					applications.push(resultJSON[i])
				return res.status(200).send(applications)
			} else {
				applications = await Application.find({ 'fields.type': "Ecommerce" }).sort({ UID: -1 })
				// applications = await Application.find({}).sort({ UID: -1 })
				client.setex('Applications', 3600, JSON.stringify({ ...applications, }));
				res.status(200).send(applications)
			}
		})
	} catch (e) {
		console.error(e)
		res.status(500).send(e)
	}
})

router.get('/admin_application_management', async (req, res) => {
	try {
		const application = await Application.find({}).sort({ UID: -1 })
		if (!application)
			return res.status(404).send({ error: true, success: false, message: "No data found", result: "" })
		return res.status(200).send({ error: false, success: true, message: "Data found", result: application })
	} catch (e) {
		console.error(e)
		res.status(500).send({ error: true, success: false, message: "Server error", result: e })
	}
})

router.get('/get_market_apps', async (req, res) => {
	try {
		const marketApps = await Application.find({}, { "fields.name": 1, "fields.desc": 1, "fields.images.thumb": 1, "fields.slug": 1, UID: 1 })
		if (!marketApps)
			return res.status(404).send({ error: true, success: false, message: "No Data Found", result: "" })
		return res.status(200).send({ error: false, success: true, message: "Found Market Apps", result: marketApps })
	} catch (error) {
		console.error(error)
		return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
	}
})

router.get('/application-management/:id', async (req, res) => {
	try {
		// const application = await Application.findOne({ UID: req.params.id })
		// res.status(200).send(application)
		return client.get(`Application-${req.params.id}`, async (err, result) => {
			if (result) {
				const application = JSON.parse(result)
				return res.status(200).send(application._doc)
			} else {
				const application = await Application.findOne({ UID: req.params.id })
				client.setex(`Application-${req.params.id}`, 3600, JSON.stringify({ source: 'Redis Cache', ...application, }))
				res.status(200).send(application)
			}
		})
	} catch (e) {
		console.error(e)
		res.status(500).send(e)
	}
})

router.get('/admin_application_management/:id', async (req, res) => {
	try {
		const applications = await Application.findOne({ UID: req.params.id })
		res.status(200).send(applications)
	} catch (error) {
		console.error(error)
		res.status(500).send(error)

	}
})

router.patch('/application-management/:id', async (req, res) => {
	try {
		console.log(req.body.fields.screenshots)
		const application = await Application.findOneAndUpdate({ UID: req.params.id }, { $set: req.body }, { new: true })
		if (!application)
			return res.status(400).send({ error: true, success: false, message: "Bad request", result: "" })
		console.log(`application with id:${req.params.id} has been updated.`)
		return res.status(200).send({ error: false, success: true, message: "Update success", result: application })
	} catch (e) {
		console.error(e)
		res.status(500).send({ error: true, success: false, message: 'server error', result: e })
	}
})

router.delete('/application-management/:id', async (req, res) => {
	try {
		const application = await Application.findOneAndDelete({ UID: req.params.id })
		if (!application)
			return res.status(400).send({ error: true, success: false, message: "Bad request", result: "" })
		console.log(`Application with id:${id} has been deleted`)
		return res.status(200).send({ error: false, success: true, message: "Deleted successfully", result: application })
	} catch (e) {
		res.status(500).send({ error: false, success: false, message: "server error", result: e })
	}
})

module.exports = router