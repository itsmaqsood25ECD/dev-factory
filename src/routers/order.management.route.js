require('dotenv').config()
const express = require('express')
const moment = require('moment')
const auth = require('../middleware/auth')
const nodemailer = require('nodemailer')
const Order = require('../models/payment.model')
const Order_Cancellation = require('../models/order.cancellation.model')
const { findOneAndUpdate } = require('../models/payment.model')

const router = new express.Router()
// Send the email
const transporter = nodemailer.createTransport({
	host: 'smtp.zoho.com',
	port: 465,
	secure: true,
	auth: {
		user: process.env.EMAIL_USERNAME,
		pass: process.env.EMAIL_PASSWORD
	}
})

router.get('/get_user_orders', auth, async (req, res) => {
	try {
		const orders = await Order.getOrderByUser(req.user._id)
		if (!orders)
			return res.status(400).send({error:true,message:"Order not found",success:false,result:[]})
		return res.status(200).send({error:false,success:true,message:"Order found",result: orders})
	} catch (error) {
		return res.status(400).send({error:true,message:"Order not found",success:false,result:[]})
	}
})

router.get('/get_user_order_details/:id', auth, async (req, res) => {
	try {
		const order = await Order.findOne({ UID: req.params.id })
		if (!order)
			return res.status(404).send({ error: true, success: false, message: "No order found", result: "" })
		return res.status(200).send({ error: false, success: true, message: "Order found", result: order })
	} catch (error) {
		console.error(error)
		return res.status(500).send({ error: true, success: false, message: "Server error", result: error })
	}
})

router.get('/get-all-orders', async (req, res) => {
	try {
		const orders = await Order.find({}).sort({ payment_initated_date: -1 }).populate('user')
		if (!orders)
			return res.status(400).send('No Orders Found')
		res.status(200).send(orders)
	} catch (error) {
		res.send(error)
	}
})

router.patch('/order/:id', auth, async (req, res) => {
	try {
		const order = await Order.findOneAndUpdate({ UID: req.params.id }, { $set: req.body }, { new: true })
		if (!order)
			return res.status(400).send({ error: true, success: false, message: "Bad request", result: "" })
		return res.status(200).send({ error: false, success: true, message: "Update success", result: order })
	} catch (e) {
		console.error(e)
		res.status(500).send({ error: true, success: false, message: 'server error', result: e })
	}
})

router.delete('/order/:id', auth, async (req, res) => {
	try {
		const order = await Order.findOneAndDelete({ UID: req.params.id })
		if (!order)
			return res.status(400).send({ error: true, success: false, message: "Bad request", result: "" })
		return res.status(200).send({ error: false, success: true, message: "Deleted successfully", result: order })
	} catch (e) {
		res.status(500).send({ error: false, success: false, message: "server error", result: e })
	}
})
router.post('/cancel-subscription/:id', auth, async (req, res) => {
	try {
		const prevCancellation = await Order_Cancellation.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID
		if (prevCancellation.length === 0) {
			counter = 1
		} else {
			counter = prevCancellation[0].UID.slice(8)
			counter = +counter + 1
		}
		if (counter < 10) {
			UID = `UAF-CAN-0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `UAF-CAN-0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `UAF-CAN-000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `UAF-CAN-00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `UAF-CAN-0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `UAF-CAN-000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `UAF-CAN-00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `UAF-CAN-0${counter}`
		} else {
			UID = `UAF-CAN-${counter}`
		}
		const OrderDetails = await Order.findById(req.params.id)
		let body
		if (OrderDetails.status.purpose === "Activation" && OrderDetails.status.state === "Active") {
			let amtToBeCharged
			let amtToBeReturned
			let numberOfMonthsUsedFor
			const subscriptionUsedFor = moment(OrderDetails.start_date).endOf().fromNow()
			if (subscriptionUsedFor.includes("day")) {
				if (OrderDetails.packType === "Year") {
					const perMonthCost = (+OrderDetails.total_price / 12).toFixed(2)
					amtToBeCharged = (+perMonthCost + ((+perMonthCost * 11) * .25)).toFixed(2)
					amtToBeReturned = +OrderDetails.total_price - +amtToBeCharged
				} else if (OrderDetails.packType === "Month") {
					amtToBeCharged = ((+OrderDetails.total_price * 11) * .25).toFixed(2)
				}
			} else if (subscriptionUsedFor.includes("month")) {
				numberOfMonthsUsedFor = subscriptionUsedFor.slice(0, 1)
				if (numberOfMonthsUsedFor === "a") {
					numberOfMonthsUsedFor = 1
				}
				if (OrderDetails.packType === "Month") {
					amtToBeCharged = (((12 - +numberOfMonthsUsedFor) * (+OrderDetails.total_price)) * .25).toFixed(2)
				} else if (OrderDetails.packType === "Year") {
					const perMonthCost = (+OrderDetails.total_price / 12).toFixed(2)
					amtToBeCharged = (perMonthCost * numberOfMonthsUsedFor) + (((12 - numberOfMonthsUsedFor) * perMonthCost) * .25)
					amtToBeReturned = +OrderDetails.total_price - +amtToBeCharged
				}
			}
			body = {
				amt_to_be_charged: amtToBeCharged,
				amt_to_be_returned: amtToBeReturned,
				user: req.user,
				order: OrderDetails,
				number_of_months_used_for: numberOfMonthsUsedFor
			}
		} else {
			body = {
				user: req.user,
				order: OrderDetails,
				number_of_months_used_for: 0
			}
		}
		const cancel_order = new Order_Cancellation(body)
		cancel_order.ticket_no = Date.now()
		cancel_order.UID = UID
		await cancel_order.save()
		const updateObj = {
			purpose: "Cancellation",
			state: "Pending"
		}
		await Order.findByIdAndUpdate(req.params.id, { status: updateObj }, { new: true })
		const mailOptions = {
			from: process.env.EMAIL_USERNAME,
			to: req.user.email,
			subject: 'Cancel Subscription',
			bcc: "contactus@upapp.co",
			html: `<!DOCTYPE html>
				<html lang="en">
					<head>
						<meta charset="UTF-8" />
						<meta
							name="viewport"
							content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
						/>
						<meta
							name="viewport"
							content="width=device-width, initial-scale=1, shrink-to-fit=no"
						/>
						<title>Cancel Subscription</title>
					</head>
					<body
						style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5"
					>
						<table
							style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px"
						>
							<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
		              <td style="
			 padding-left: 15px;
		">
		                  <a href="">
		                      <img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
		                  </a>
		              </td>

		          </tr>
								<tr>
									<td
										style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;"
									>
										Dear ${req.user.firstName} ${req.user.lastName},
										<br /><br />
										Thank you for being UPappfactory’s esteemed customer and we
										understand you’ve decided to cancel your subscription with UPApp
										Factory. We are sad to know this, and we will surely help you with
										the process with no hassles as always promised.
										<br /><br />
										Please note that as per the policies as mentioned on
										<a href="https://www.upappfactory.com/cancelation-refund-policy"
											>https://www.upappfactory.com/cancelation-refund-policy</a
										>. <br /><br />
										Our support team will ensure the refund if any as per the policy and
										we will go ahead to cancel your subscription.
										<br /><br />
										If you would like to speak with one of our support representatives,
										then please reach out to us at
										<a href="mailto:support@upappfactory.com"
											>support@upappfactory.com</a
										>. <br /><br />
										We would highly request if you can advise us the reason of your
										cancelation with your feedback which can help us improve our
										services for you in the future at
										<a href="mailto:support@upappfactory.com"
											>support@upappfactory.com</a
										>. <br /><br />
										We look forward to seeing you as our esteemed customer again very
										soon.
										<br /><br />
										Regards,
										<br />
										Support Team
										<br />
										UPappfactory
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">

								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>

							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
							</tbody>
						</table>
					</body>
				</html>

				 `
		}
		await transporter.sendMail(mailOptions)
		const notify = {
			from: process.env.EMAIL_USERNAME,
			to: "contactus@upapp.co",
			subject: `Cancel Subscription/Order from ${req.user.firstName} ${req.user.lastName} of ${OrderDetails.appName}`,
			bcc: "ali@upappfactory.com, aslam@upappfactory.com, nour@upappfactory.com, zakaria@upappfactory.com, saad@upappfactory.com, tejas@upappfactory.com",
			html: `<!DOCTYPE html>
				<html lang="en">

				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Cancel Subscription/Order</title>
					<style>
						.bank td,
						th {
							border: 1px solid #dddddd;
							text-align: left;
							padding: 8px;
						}

						.bank tr:nth-child(even) {
							background-color: #dddddd;
						}
					</style>
				</head>

				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="
								 padding-left: 15px;
							">
									<a href="">
										<img style="width: 150px" width="150"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>

							</tr>
							<tr>
								<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
									Hi Sales Team,
									<br>
									We have received a subscription cancellation request for ticket no: ${cancel_order.UID}, please find the
									details
									<br>
									<br>
									<table class="bank" style="width:100%;border-spacing: 0px;border:none">

										<tr>
											<td>Ticket No</td>
											<td>${cancel_order.UID}</td>
										</tr>

										<tr>
											<td>Name</td>
											<td>${req.user.firstName} ${req.user.lastName}</td>
										</tr>
										<tr>
											<td>Email Address</td>
											<td>${req.user.email}</td>
										</tr>
										<tr>
											<td>Phone No</td>
											<td>${req.user.phone}</td>
										</tr>
										<tr>
											<td>Country</td>
											<td>${req.user.country}</td>
										</tr>
										<tr>
											<td>App Name</td>
											<td>${OrderDetails.appName}</td>
										</tr>
										<tr>
											<td>Subscription Type</td>
											<td>${OrderDetails.packType}</td>
										</tr>
										<tr>
											<td>Subscription Start Date</td>
											<td>${moment(OrderDetails.start_date).format('L')}</td>
										</tr>
										<tr>
											<td>Subscription End Date</td>
											<td>${moment(OrderDetails.end_date).format('L')}</td>
										</tr>

									</table>

									<br />
									Thanks!
									<br>
									<br />
									Regards,
									<br />
									UPapp factory
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">

								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>

							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>

				</html> `
		}
		await transporter.sendMail(notify)
		return res.status(201).send(OrderDetails)

	} catch (error) {
		console.error(error)
		res.status(400).send(error)
	}
})

router.get("/cancel-subscription", async (req, res) => {
	try {
		const cancelledOrders = await Order_Cancellation.find({}).populate("user").populate("order")
		res.status(200).send(cancelledOrders)
	} catch (error) {
		console.error(error)
	}
})

router.patch('/cancel-subscription/:id', async (req, res) => {
	try {
		const cancelledOrders = await Order_Cancellation.findByIdAndUpdate(req.params.id, { pending: false }, { new: true }).populate('user')
		await Order.findByIdAndUpdate(cancelledOrders.order, { status: { purpose: "Cancellation", state: "Approved" } }, { new: true })
		const user = cancelledOrders.user

		const mailOptions = {
			from: process.env.EMAIL_USERNAME,
			to: user.email,
			subject: 'Final Invoice',
			bcc: "contactus@upapp.co",
			html: `<!DOCTYPE html>
			<html lang="en">
				<head>
					<meta charset="UTF-8" />
					<meta
						name="viewport"
						content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
					/>
					<meta
						name="viewport"
						content="width=device-width, initial-scale=1, shrink-to-fit=no"
					/>
					<title>Cancel Subscription</title>
				</head>
				<body
					style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5"
				>
					<table
						style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;"
					>
						<tbody>
						<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
						<td style="
				 padding-left: 15px;
			">
							<a href="">
								<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
							</a>
						</td>

					</tr>
							<tr>
								<td
									style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;"
								>
									Dear ${user.firstName} ${user.lastName},
									This is your final invoice. Amt to be paid:${cancelledOrders.amt_to_be_charged} and amt to be returned:${cancelledOrders.amt_to_be_returned} <br /><br />
									We look forward to seeing you as our esteemed customer again very
									soon.
									<br /><br />
									Regards,
									<br />
									Support Team
									<br />
									UPappfactory
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">

							<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
								<p style="margin: 10px auto 0px auto;line-height: 0.8;">
									<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
									|
									<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
								</p>
								<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
								<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
								<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
							</td>

						</tr>
						<tr style="padding:20px;text-align: center;background: #f7f7f7">
							<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
								Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
							</td>
						</tr>
						</tbody>
					</table>
				</body>
			</html>
			 `
		}
		await transporter.sendMail(mailOptions)
		res.status(200).send(cancelledOrders)
	} catch (error) {
		console.error(error)
		res.send(400).send(error)
	}
})

router.delete('/cancel-subscription/:id', async (req, res) => {
	try {
		const cancelledOrders = await Order_Cancellation.findById(req.params.id)
		const Orders = await Order.findByIdAndUpdate(cancelledOrders.order, { status: { purpose: "Activation", state: "Active" } }, { new: true })
		await cancelledOrders.remove()
		res.status(200).send(Orders)
	} catch (error) {
		console.error(error)
		res.send(400).send(error)
	}
})

module.exports = router