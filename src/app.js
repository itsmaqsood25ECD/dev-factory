require('dotenv').config()
require('./services/mongoose.service')
var compression = require('compression')
const express = require('express')
const path = require("path")
const cors = require('cors')
const axios = require('axios')
const bodyParser = require('body-parser')
const userRouter = require('./routers/user.route')
const paymentRouter = require('./routers/payment.route')
const couponManagementRouter = require('./routers/coupon.management.route')
const applicationManagementRouter = require('./routers/application.management.route')
const contactRouter = require('./routers/contact.route')
const categoryManagementRouter = require('./routers/category.management.route')
const orderManagementRouter = require('./routers/order.management.route')
const careerManagementRouter = require('./routers/careers.route')
const fileUpload = require('./routers/file.upload.route')
const partnerRouter = require('./routers/partners.route')
const eventRequestRouter = require('./routers/events.request.route')
const checkoutRouter = require('./routers/checkout.route.js')
const subscriptionRouter = require('./routers/subscription.route')
const blogRouter = require('./routers/blogs.management.route')
const studioContactRouter = require('./routers/studio.contact.route')

const app = express()
const port = process.env.PORT || 5000;
// maintainance middleware
// app.use((req,res,next) => {
//     res.status(503).send('Site is under maintainance. Please check later')
// })
app.use(compression())
app.use(express.json({ extended: false, limit: '10mb' }))
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
app.use(userRouter)
app.use(contactRouter)
app.use(paymentRouter)
app.use(couponManagementRouter)
app.use(applicationManagementRouter)
app.use(categoryManagementRouter)
app.use(fileUpload)
app.use(orderManagementRouter)
app.use(careerManagementRouter)
app.use(partnerRouter)
app.use(eventRequestRouter)
app.use(checkoutRouter)
app.use(subscriptionRouter)
app.use(blogRouter)
app.use(studioContactRouter)

if (process.env.NODE_ENV === 'production') {

    app.use(express.static('client/build'))
    app.get('*', (req, res) => {
        // let url = path.join(__dirname, '../client/build', 'index.html');
        // if (!url.startsWith('/app/')) // since we're on local windows
        //     url = url.substring(1);
        res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
    })
}

app.listen(port, () => {
    console.log(`app running at port ${port}`)
})
